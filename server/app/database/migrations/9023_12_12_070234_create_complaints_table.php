<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->date('failure_date'); // Дата отказа
            $table->float('operating_hours'); // Наработка, м/час
            $table->text('failure_description'); // Описание отказа
            $table->text('used_spare_parts'); // Используемые запасные части
            $table->date('recovery_date'); // Дата восстановления
            $table->integer('downtime_days'); // Время простоя техники
            $table->timestamps();

            // Внешние ключи
            $table->foreignId('machine_id')->references('id')->on('machines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('failure_component_id')->references('id')->on('failure_components')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('repair_method_id')->references('id')->on('repair_methods')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('complaints');
    }
};
