<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->id();
            $table->string('machine_number'); // Зав. № машины - уникальный номер машины
            $table->string('engine_number'); // Зав. № двигателя
            $table->string('transmission_number'); // Зав. № трансмиссии
            $table->string('axle_number'); // Зав. № ведущего моста
            $table->string('steerable_axle_number'); // Зав. № управляемого моста
            $table->string('delivery_agreement_number'); //№ договор поставки
            $table->date('delivery_agreement_date'); //дата поставки
            $table->date('shipping_date'); // Дата отгрузки с завода
            $table->string('consignee'); // Грузополучатель (конечный потребитель)
            $table->string('delivery_address'); // Адрес поставки (эксплуатации)
            $table->string('equipment_configuration'); // Комплектация (доп. опции)

            $table->timestamps();

            $table->foreignId('equipment_model_id')->references('id')->on('equipment_models')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('engine_model_id')->references('id')->on('engine_models')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('transmission_model_id')->references('id')->on('transmission_models')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('axle_model_id')->references('id')->on('axle_models')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('steerable_axle_model_id')->references('id')->on('steerable_axle_models')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('machines');
    }
};
