<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('technical_services', function (Blueprint $table) {
            $table->id();
            $table->date('ts_date'); // Дата проведения ТО
            $table->float('operating_hours'); // Наработка, м/час
            $table->string('work_order_number'); // № заказ-наряда
            $table->date('work_order_date'); // Дата заказ-наряда

            $table->foreignId('machine_id')->references('id')->on('machines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('ts_type_id')->references('id')->on('technical_service_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('technical_services');
    }
};
