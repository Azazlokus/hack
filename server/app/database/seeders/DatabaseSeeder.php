<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\AxleModel;
use App\Models\FailureComponent;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(UserSeeder::class);
        $this->call(AxleSeeder::class);
        $this->call(EngineSeeder::class);
        $this->call(EquipmentSeeder::class);
        $this->call(FailureComponentSeeder::class);
        $this->call(RepairMethodSeeder::class);
        $this->call(SteerableAxleSeeder::class);
        $this->call(TechnicalServiseTypesSeeder::class);
        $this->call(TransmissionSeeder::class);
        $this->call(MachineSeeder::class);
        $this->call(ComplaintSeeder::class);
        $this->call(TechnicalServiceSeeder::class);

    }
}
