<?php

namespace Database\Seeders;

use App\Models\TransmissionModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TransmissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'HF50-VP020' => 'Hyundai (Корея)',
            '10VB-00106' => 'MS (Корея)',
            '10VA-00105' => 'MS (Корея)',
            'HF30-VP010' => 'Hyundai (Корея)',
        ];

        foreach ($data as $name => $description) {
            TransmissionModel::create([
                'name' => $name,
                'description' => $description,
            ]);
        }
    }
}
