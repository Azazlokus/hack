<?php

namespace Database\Seeders;

use App\Models\AxleModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AxleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            '20VA-00101' => 'MS (Корея)',
            '20VB-00102' => 'MS (Корея)',
            'HA30-02020' => 'Hyundai (Корея)',
            'HA50-VP010' => 'Hyundai (Корея)',
        ];

        foreach ($data as $name => $description) {
            AxleModel::create([
                'name' => $name,
                'description' => $description,
            ]);
        }
    }
}
