<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

       User::create([
           'name' => 'client1',
           'login' => 'Client',
           'role' => 'Клиент',
           'password' => Hash::make('password'),
           'remember_token' => Str::random(10),
           'created_at' => now(),
           'updated_at' => now(),
       ]);
        User::create([
            'name' => 'Client2',
            'login' => 'Client2',
            'role' => 'Клиент',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'company1',
            'login' => 'Company',
            'role' => 'Сервисная организация',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'manager1',
            'login' => 'Manager',
            'role' => 'Менеджер',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'admin1',
            'login' => 'Admin',
            'role' => 'Администратор',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'Test',
            'login' => 'Test',
            'role' => 'Клиент',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'ООО Силант',
            'login' => 'Silant',
            'role' => 'Сервисная организация',
            'description' => 'г.Чебоксары, тел. +7835212334566',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'Самостоятельно',
            'login' => 'Yourself',
            'role' => 'Сервисная организация',
            'description' => 'клиент самостоятельно проводит ТО',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'ООО ФНС',
            'login' => 'FNS',
            'role' => 'Сервисная организация',
            'description' => 'г.Москва, тел. +7496444995',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        User::create([
            'name' => 'ООО Промышленная техника',
            'login' => 'PromTech',
            'role' => 'Сервисная организация',
            'description' => 'г.Чебоксары, тел. +783529994433',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
