<?php

namespace Database\Seeders;

use App\Models\TechnicalService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TechnicalServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TechnicalService::create([
            'ts_type_id' => 1, // Вид ТО
            'ts_date' => '2020-10-10', // Дата проведения ТО
            'operating_hours' => '11', // Наработка, м/час
            'work_order_number' => '1234', // № заказ-наряда
            'work_order_date' => '2020-09-10', // Дата заказ-наряда
            'machine_id' => 1,
            'service_id' => 3,
        ]);
        TechnicalService::create([
            'ts_type_id' => 1, // Вид ТО
            'ts_date' => '2020-10-10', // Дата проведения ТО
            'operating_hours' => '11', // Наработка, м/час
            'work_order_number' => '1234', // № заказ-наряда
            'work_order_date' => '2020-10-10', // Дата заказ-наряда
            'machine_id' => 2,
            'service_id' => 3,

        ]);
        TechnicalService::create([
            'ts_type_id' => 2, // Вид ТО
            'ts_date' => '2020-10-10', // Дата проведения ТО
            'operating_hours' => '11', // Наработка, м/час
            'work_order_number' => '1234', // № заказ-наряда
            'work_order_date' => '2020-09-10', // Дата заказ-наряда
            'machine_id' => 3,
            'service_id' => 2,

        ]);
    }
}
