<?php

namespace Database\Seeders;

use App\Models\RoleUser;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        RoleUser::create([
            'user_id' => 1,
            'role_id' => 1
        ]);
        RoleUser::create([
            'user_id' => 2,
            'role_id' => 2
        ]);
        RoleUser::create([
            'user_id' => 3,
            'role_id' => 3
        ]);
        RoleUser::create([
            'user_id' => 4,
            'role_id' => 4
        ]);
        RoleUser::create([
            'user_id' => 5,
            'role_id' => 1
        ]);
    }
}
