<?php

namespace Database\Seeders;

use App\Models\FailureComponent;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FailureComponentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name' => 'Двигатель', 'description' => 'Описание для двигателя'],
            ['name' => 'Трансмиссия', 'description' => 'Описание для трансмиссии'],
            ['name' => 'Ведущий мост', 'description' => 'Описание для ведущего моста'],
            ['name' => 'Управляемый мост', 'description' => 'Описание для управляемого моста'],
            ['name' => 'Подъёмное устройство', 'description' => 'Описание для подъёмного устройства'],
            ['name' => 'Гидросистема', 'description' => 'Описание для гидросистемы'],
        ];

        foreach ($data as $equipment) {
            FailureComponent::create([
                'name' => $equipment['name'],
                'description' => $equipment['description'],
            ]);
        }
    }
}
