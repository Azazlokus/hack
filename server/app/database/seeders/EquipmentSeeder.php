<?php

namespace Database\Seeders;

use App\Models\EquipmentModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EquipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'ПД1,5' => '',
            'ПД5,0' => '',
            'ПГ1,5' => '',
            'ПД2,0' => '',
            'ПД2,5' => '',
            'ПД3,0' => '',
        ];

        foreach ($data as $name => $description) {
            EquipmentModel::create([
                'name' => $name,
                'description' => $description,
            ]);
        }
    }
}
