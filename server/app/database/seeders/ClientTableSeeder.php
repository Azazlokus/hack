<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Client::create([
           'name' => 'Client1',
           'description' => 'Some description for client1'
        ]);
        Client::create([
            'name' => 'Client2',
            'description' => 'Some description for client2'
        ]);
        Client::create([
            'name' => 'Client3',
            'description' => 'Some description for client3'
        ]);
        Client::create([
            'name' => 'Test',
            'description' => 'Some description for client3'
        ]);
    }
}
