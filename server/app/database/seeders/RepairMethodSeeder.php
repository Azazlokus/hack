<?php

namespace Database\Seeders;

use App\Models\RepairMethod;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RepairMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $data = [
            'Ремонт узла',
            'Замена узла',
        ];

        foreach ($data as $name) {
            RepairMethod::create([
                'name' => $name,
            ]);
        }

    }
}
