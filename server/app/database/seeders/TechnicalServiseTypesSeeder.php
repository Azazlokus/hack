<?php

namespace Database\Seeders;

use App\Models\TechnicalServiceType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TechnicalServiseTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'ТО-0 (50 м/час)' => 'Описание для ТО-0 (50 м/час)',
            'ТО-1 (200 м/час)' => 'Описание для ТО-1 (200 м/час)',
            'ТО-2 (400 м/час)' => 'Описание для ТО-2 (400 м/час)',
            'ТО-3 (600 м/час)' => 'Описание для ТО-3 (600 м/час)',
            'ТО-4 (1000м/час)' => 'Описание для ТО-4 (1000м/час)',
            'ТО-5 (2000м/час)' => 'Описание для ТО-5 (2000м/час)',
        ];

        foreach ($data as $name => $description) {
            TechnicalServiceType::create([
                'name' => $name,
                'description' => $description,
            ]);
        }
    }
}
