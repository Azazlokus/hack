<?php

namespace Database\Seeders;

use App\Models\Complaint;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ComplaintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Complaint::create([
            'failure_date' => '2020-01-02', // Дата отказа
            'operating_hours' => 123.1, // Наработка, м/час
            'failure_component_id' => 1, // Узел отказа
            'failure_description' => 'Had broken', // Описание отказа
            'repair_method_id' => 2,// Способ восстановления
            'used_spare_parts' => '1 2 3', // Используемые запасные части
            'recovery_date' => '2020-01-10',// Дата восстановления
            'machine_id' => 1,
            'service_id' => 2,

        ]);
        Complaint::create([
            'failure_date' => '2022-11-02', // Дата отказа
            'operating_hours' => 22, // Наработка, м/час
            'failure_component_id' => 1, // Узел отказа
            'failure_description' => 'Had broken', // Описание отказа
            'repair_method_id' => 1,// Способ восстановления
            'used_spare_parts' => '2', // Используемые запасные части
            'recovery_date' => '2023-01-10',// Дата восстановления
            'machine_id' => 2,
            'service_id' => 4,

        ]);
        Complaint::create([
            'failure_date' => '2021-02-02', // Дата отказа
            'operating_hours' => 12, // Наработка, м/час
            'failure_component_id' => 3, // Узел отказа
            'failure_description' => 'Had broken', // Описание отказа
            'repair_method_id' => 1,// Способ восстановления
            'used_spare_parts' => '2', // Используемые запасные части
            'recovery_date' => '2023-01-10',// Дата восстановления
            'machine_id' => 3,
            'service_id' => 2,

        ]);
    }
}
