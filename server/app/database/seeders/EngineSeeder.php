<?php

namespace Database\Seeders;

use App\Models\EngineModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EngineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $enginesData = [
            'Kubota D1803' => 'Кубота (Япония)',
            'Kubota V2403' => 'Кубота (Япония)',
            'Kubota V3300' => 'Кубота (Япония)',
            'Nissan K21' => 'Ниссан (Япония)',
            'MMZ-3LD' => 'ММЗ (Беларусь)',
            'MMZ-3LDT' => 'ММЗ (Беларусь)',
            'MMZ-4D' => 'ММЗ (Беларусь)',
            'MMZ-4DT' => 'ММЗ (Беларусь)',
            'ММЗ Д-243' => 'ММЗ (Беларусь)',
            'Тяговый э/двигатель' => 'электродвигатель, Китай',
        ];

        foreach ($enginesData as $name => $description) {
            EngineModel::create([
                'name' => $name,
                'description' => $description,
            ]);
        }
    }
}
