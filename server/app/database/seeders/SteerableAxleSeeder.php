<?php

namespace Database\Seeders;

use App\Models\SteerableAxleModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SteerableAxleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'VS20-00001' => 'MS (Корея)',
            'VS30-00001' => 'MS (Корея)',
            'B350655A' => 'Hyundai (Корея)',
            'E180-30.10.000' => 'Hyundai (Корея)',
        ];

        foreach ($data as $name => $description) {
            SteerableAxleModel::create([
                'name' => $name,
                'description' => $description,
            ]);
        }
    }
}
