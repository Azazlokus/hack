<?php

namespace Database\Seeders;

use App\Models\Machine;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MachineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Machine::create([
            'machine_number' => '123', // Зав. № машины - уникальный номер машины
            'equipment_model_id' => 1, // Модель техники
            'engine_model_id' => 1, // Модель двигателя
            'engine_number' => '123', // Зав. № двигателя
            'transmission_model_id' => 1, // Модель трансмиссии
            'transmission_number' => '123',  // Зав. № трансмиссии
            'axle_model_id' => 1, // Модель ведущего моста
            'axle_number' => '123',// Зав. № ведущего моста
            'steerable_axle_model_id' => 1, // Модель управляемого моста
            'steerable_axle_number' => '123', // Зав. № управляемого моста
            'delivery_agreement_number' => '123s',
            'delivery_agreement_date' => '2020-10-03',
            'shipping_date' => '2023-01-01', // Дата отгрузки с завода
            'consignee' => 'Apple', // Грузополучатель (конечный потребитель)
            'delivery_address' => 'Tomsk', // Адрес поставки (эксплуатации)
            'equipment_configuration' => 'none',// Комплектация (доп. опции)
            'service_id' => 2,
            'client_id' => 3
        ]);

        Machine::create([
            'machine_number' => '124', // Зав. № машины - уникальный номер машины
            'equipment_model_id' => 1, // Модель техники
            'engine_model_id' => 3, // Модель двигателя
            'engine_number' => '123', // Зав. № двигателя
            'transmission_model_id' => 2, // Модель трансмиссии
            'transmission_number' => '123',  // Зав. № трансмиссии
            'axle_model_id' => 2, // Модель ведущего моста
            'axle_number' => '124',// Зав. № ведущего моста
            'steerable_axle_model_id' => 2, // Модель управляемого моста
            'steerable_axle_number' => '124', // Зав. № управляемого моста
            'delivery_agreement_number' => '124s',
            'delivery_agreement_date' => '2020-10-21',
            'shipping_date' => '2021-02-12', // Дата отгрузки с завода
            'consignee' => 'Huawei', // Грузополучатель (конечный потребитель)
            'delivery_address' => 'Moscow', // Адрес поставки (эксплуатации)
            'equipment_configuration' => 'full',// Комплектация (доп. опции)
            'service_id' => 4,
            'client_id' => 3
        ]);
        Machine::create([
            'machine_number' => '125', // Зав. № машины - уникальный номер машины
            'equipment_model_id' => 1, // Модель техники
            'engine_model_id' => 2, // Модель двигателя
            'engine_number' => '125', // Зав. № двигателя
            'transmission_model_id' => 2, // Модель трансмиссии
            'transmission_number' => '125',  // Зав. № трансмиссии
            'axle_model_id' => 2, // Модель ведущего моста
            'axle_number' => 125,// Зав. № ведущего моста
            'steerable_axle_model_id' => 3, // Модель управляемого моста
            'steerable_axle_number' => '123d', // Зав. № управляемого моста
            'delivery_agreement_number' => '125s',
            'delivery_agreement_date' => '2022-10-01',
            'shipping_date' => '2022-01-01', // Дата отгрузки с завода
            'consignee' => 'MacDonalds', // Грузополучатель (конечный потребитель)
            'delivery_address' => 'Kemerovo', // Адрес поставки (эксплуатации)
            'equipment_configuration' => 'full',// Комплектация (доп. опции)
            'service_id' => 2,
            'client_id' => 2
        ]);
    }
}
