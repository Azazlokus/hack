<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ComplaintController;
use App\Http\Controllers\MachineController;
use App\Http\Controllers\References\AxelModelController;
use App\Http\Controllers\References\EngineModelController;
use App\Http\Controllers\References\EquipmentModelController;
use App\Http\Controllers\References\FailureComponentController;
use App\Http\Controllers\References\MaintenanceOrganizationController;
use App\Http\Controllers\References\RepairMethodController;
use App\Http\Controllers\References\SteerableAxleModelController;
use App\Http\Controllers\References\TechnicalServiceTypeController;
use App\Http\Controllers\References\TransmissionModelController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\TechnicalServiceController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Orion\Facades\Orion;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
});

Orion::resource('users', UserController::class);
Orion::resource('machines', MachineController::class);
Orion::resource('complaints', ComplaintController::class);
Orion::resource('technical-services', TechnicalServiceController::class);
Orion::resource('axle-models', AxelModelController::class);
Orion::resource('engine-models', EngineModelController::class);
Orion::resource('equipment-models', EquipmentModelController::class);
Orion::resource('failure-components', FailureComponentController::class);
Orion::resource('maintenance-organizations', MaintenanceOrganizationController::class);
Orion::resource('repair-methods', RepairMethodController::class);
Orion::resource('steerable-axle-models', SteerableAxleModelController::class);
Orion::resource('technical-service-types', TechnicalServiceTypeController::class);
Orion::resource('transmission-models', TransmissionModelController::class);
Orion::resource('services', ServiceController::class);
Orion::resource('clients', ClientController::class);
