<?php

namespace App\Policies;

use App\Models\Complaint;
use App\Models\Machine;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ComplaintPolicy
{
    use HandlesAuthorization;

    public function before(User $user, string $ability): bool|null
    {
        if ($user->isAdmin()) {
            return true;
        }
        if ($user->isManager()) {
            return true;
        }
        return null;
    }

    public function viewAny(User $user): Response
    {
        if ($user->isClient()) {
            return $this->allow();
        }
        if ($user->isService()) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function view(User $user, Complaint $complaint): Response
    {
        if ($user->isClient() and $user->id == $complaint->service->user_id) {
            return $this->allow();
        }
        if ($user->isService() and $user->id == $complaint->service->user_id) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function create(User $user): Response
    {

        if ($user->isService()) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function update(User $user, Complaint $complaint): Response
    {
        if ($user->isService() and $user->id == $complaint->service->user_id) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function delete(User $user, Complaint $complaint): Response
    {

        if ($user->isService() and $user->id == $complaint->service->user_id) {
            return $this->allow();
        }

        return $this->deny();
    }

    public function restore(User $user, Complaint $complaint): Response
    {

        if ($user->isService() and $user->id == $complaint->service->user_id) {
            return $this->allow();
        }

        return $this->deny();
    }

    public function forceDelete(User $user, Complaint $complaint): Response
    {

        if ($user->isService() and $user->id == $complaint->service->user_id) {
            return $this->allow();
        }

        return $this->deny();
    }
}
