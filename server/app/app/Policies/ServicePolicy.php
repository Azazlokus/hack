<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ServicePolicy
{
    use HandlesAuthorization;

    public function before(User $user, string $ability): bool|null
    {
        if ($user->isAdmin()) {
            return true;
        }

        return null;
    }

    public function viewAny(User $user): Response
    {
        return $this->deny();

    }

    public function view(User $user): Response
    {
        return $this->deny();
    }

    public function create(User $user): Response
    {

        return $this->deny();
    }

    public function update(User $user): Response
    {

        return $this->deny();
    }

    public function delete(User $user): Response
    {

        return $this->deny();
    }

    public function restore(User $user): Response
    {

        return $this->deny();
    }

    public function forceDelete(User $user): Response
    {

        return $this->deny();
    }
}
