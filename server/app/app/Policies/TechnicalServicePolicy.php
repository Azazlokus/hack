<?php

namespace App\Policies;

use App\Models\TechnicalService;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class TechnicalServicePolicy
{
    use HandlesAuthorization;

    public function before(User $user, string $ability): bool|null
    {
        if ($user->isAdmin()) {
            return true;
        }
        if ($user->isManager()) {
            return true;
        }
        return null;
    }

    public function viewAny(User $user): Response
    {
        if ($user->isClient()) {
            return $this->allow();
        }
        if ($user->isService()) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function view(User $user, TechnicalService $technicalService): Response
    {
        if ($user->isClient() and $user->id == $technicalService->machine->client->user_id) {
            return $this->allow();
        }
        if ($user->isService() and $user->id == $technicalService->machine->service->user_id) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function create(User $user): Response
    {
        if ($user->isClient()) {
            return $this->allow();
        }
        if ($user->isService()) {
            return $this->allow();
        }

        return $this->deny();
    }

    public function update(User $user, TechnicalService $technicalService): Response
    {
        if ($user->isClient() and $user->id == $technicalService->machine->client->user_id) {
            return $this->allow();
        }
        if ($user->isService() and $user->id == $technicalService->machine->service->user_id) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function delete(User $user, TechnicalService $technicalService): Response
    {
        if ($user->isClient() and $user->id == $technicalService->machine->client->user_id) {
            return $this->allow();
        }
        if ($user->isService() and $user->id == $technicalService->machine->service->user_id) {
            return $this->allow();
        }

        return $this->deny();
    }

    public function restore(User $user, TechnicalService $technicalService): Response
    {
        if ($user->isClient() and $user->id == $technicalService->machine->client->user_id) {
            return $this->allow();
        }
        if ($user->isService() and $user->id == $technicalService->machine->service->user_id) {
            return $this->allow();
        }

        return $this->deny();
    }

    public function forceDelete(User $user, TechnicalService $technicalService): Response
    {
        if ($user->isClient() and $user->id == $technicalService->machine->client->user_id) {
            return $this->allow();
        }
        if ($user->isService() and $user->id == $technicalService->machine->service->user_id) {
            return $this->allow();
        }

        return $this->deny();
    }
}
