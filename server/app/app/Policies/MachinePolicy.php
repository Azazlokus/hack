<?php

namespace App\Policies;

use App\Models\Machine;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class MachinePolicy
{
    use HandlesAuthorization;

    public function before(User $user, string $ability): bool|null
    {
        if ($user->isAdmin()) {
            return true;
        }
        return null;
    }

    public function viewAny(?User $user): Response
    {
        return $this->allow();

    }

    public function view(?User $user, Machine $machine): Response
    {
        return $this->allow();
    }

    public function create(User $user): Response
    {
        if ($user->isManager()) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function update(User $user, Machine $machine): Response
    {
        if ($user->isManager()) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function delete(User $user, Machine $machine): Response
    {
        if ($user->isManager()) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function restore(User $user, Machine $machine): Response
    {
        if ($user->isManager()) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function forceDelete(User $user, Machine $machine): Response
    {
        if ($user->isManager()) {
            return $this->allow();
        }
        return $this->deny();
    }
}
