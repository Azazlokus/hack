<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $user, string $ability): bool|null
    {
        if ($user->isAdmin()) {
            return true;
        }
        if ($user->name == 'Самостоятельно'){
            return false;
        }
        return null;
    }
    public function viewAny(User $user): Response
    {
        return $this->allow();

    }

    public function view(User $user, User $user1): Response
    {
        if($user1->name == 'Самостоятельно'){
            return $this->deny();
        }
        if($user->id == $user1->id) {
            return $this->allow();
        }
        return $this->deny();
    }

    public function create(User $user): Response
    {
        if($user->isAdmin()){
            return $this->allow();

        }
        return $this->deny();
    }

    public function update(User $user, User $user1): Response
    {
        if($user1->name == 'Самостоятельно'){
            return $this->deny();
        }
        return $this->deny();
    }

    public function delete(User $user): Response
    {

        return $this->deny();
    }

    public function restore(User $user): Response
    {

        return $this->deny();
    }

    public function forceDelete(User $user): Response
    {

        return $this->deny();
    }
}
