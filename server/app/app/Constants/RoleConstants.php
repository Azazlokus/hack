<?php

namespace App\Constants;
enum RoleConstants
{
    const ADMIN = 'Администратор';
    const CLIENT = 'Клиент';
    const MANAGER = 'Менеджер';
    const COMPANY = 'Сервисная организация';
}
