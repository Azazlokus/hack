<?php

namespace App\Http\Requests;

use App\Constants\RoleConstants;
use Orion\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //'name' => 'required|string|min:1|max:255',
            //'login' => 'required|string|min:1|max:255',
            //'email' => 'required|email|unique:users,email|max:255',
            //'password' => 'required|string|min:8',
            //'role' => ['required', Rule::in(RoleConstants::toValues())]
        ];
    }
}
