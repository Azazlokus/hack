<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class TechnicalServiceRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'ts_type_id' => 'required', // Вид ТО
            'ts_date' => 'required' , // Дата проведения ТО
            'operating_hours' => 'required', // Наработка, м/час
            'work_order_number' => 'required', // № заказ-наряда
            'work_order_date' => 'required', // Дата заказ-наряда

            // Внешний ключ для машины, ссылается на таблицу машин
            'machine_id' => 'required',
            'service_id' => 'required'
        ];
    }

}
