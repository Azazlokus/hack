<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;
class MachineRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'machine_number' => 'required|unique:machines', // Уникальность номера машины
            'equipment_model_id' => 'required',
            'engine_model_id' => 'required',
            'engine_number' => 'required',
            'transmission_model_id' => 'required',
            'transmission_number' => 'required',
            'axle_model_id' => 'required',
            'axle_number' => 'required',
            'steerable_axle_model_id' => 'required',
            'steerable_axle_number' => 'required',
            'delivery_agreement_number' => 'required',
            'delivery_agreement_date' => 'required|date',
            'shipping_date' => 'required|date',
            'consignee' => 'required',
            'delivery_address' => 'required',
            'equipment_configuration' => 'required',
            'client_id' => 'required',
            'service_id' => 'required',
            // Другие правила валидации для других полей...
        ];
    }
}
