<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Policies\UserPolicy;
use Illuminate\Database\Eloquent\Builder;
use Orion\Concerns\DisableAuthorization;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class UserController extends Controller
{
    protected $model = User::class;
    protected $policy = UserPolicy::class;

    protected function buildIndexFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildIndexFetchQuery($request, $requestedRelations);
        $user = auth()->user();

        if (!$user->isAdmin()) {
            $query->where('id', $user->id)
            ->where('name' != 'Самостоятельно');
        }
        if ($user->isAdmin()) {
            $query->where('name', '!=', 'Самостоятельно');
        }
        return $query;
    }
}
