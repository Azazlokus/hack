<?php

namespace App\Http\Controllers;

use App\Http\Requests\TechnicalServiceRequest;
use App\Models\Machine;
use App\Models\TechnicalService;
use App\Policies\TechnicalServicePolicy;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;
use Illuminate\Database\Eloquent\Builder;

class TechnicalServiceController extends Controller
{
    protected $request = TechnicalServiceRequest::class;
    protected $model = TechnicalService::class;
    protected $policy = TechnicalServicePolicy::class;
    protected $fields = [
        'id',
        'ts_type_id', // Вид ТО
        'ts_date', // Дата проведения ТО
        'operating_hours', // Наработка, м/час
        'work_order_number', // № заказ-наряда
        'work_order_date', // Дата заказ-наряда
        'maintenance_organization', // Организация, проводившая ТО

        // Внешний ключ для машины, ссылается на таблицу машин
        'machine_id',
        'service_id'
    ];

    public function includes(): array
    {
        return [
            'tsType',
            'machine',
            'service'
        ];
    }

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);

        $user = auth()->user();
        if ($user->isClient()) {
            $query->whereIn('machine_id',  Machine::query()->where('client_id', $user->client->id)->pluck('id'));
        }
        if ($user->isService()) {
            $query->where('service_id',  $user->service->id);
        }
        return $query;
    }

    public function exposedScopes(): array
    {
        return $this->fields;
    }

    public function filterableBy(): array
    {
        return $this->fields;
    }

    public function searchableBy(): array
    {
        return $this->fields;
    }

    public function sortableBy(): array
    {
        return $this->fields;
    }

    public function aggregates(): array
    {
        return $this->fields;
    }
}
