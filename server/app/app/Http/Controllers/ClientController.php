<?php

namespace App\Http\Controllers;


use App\Models\Client;
use App\Policies\ClientPolicy;
use Orion\Concerns\DisableAuthorization;
use Orion\Http\Controllers\Controller;

class ClientController extends Controller
{
    use DisableAuthorization;

    protected $model = Client::class;
   // protected $policy = ClientPolicy::class;

    protected array $fields = [
        'name',
        'description'
    ];
    public function exposedScopes(): array
    {
        return $this->fields;
    }

    public function filterableBy(): array
    {
        return $this->fields;
    }

    public function searchableBy(): array
    {
        return $this->fields;
    }

    public function sortableBy(): array
    {
        return $this->fields;
    }

    public function aggregates(): array
    {
        return $this->fields;
    }
}
