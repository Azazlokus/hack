<?php

namespace app\Http\Controllers\References;

use App\Models\SteerableAxleModel;
use Orion\Concerns\DisableAuthorization;
use Orion\Http\Controllers\Controller;

class SteerableAxleModelController extends Controller
{
    use DisableAuthorization;
    protected $model = SteerableAxleModel::class;

    protected array $fields = [
        'name',
        'description'
    ];
    public function exposedScopes(): array
    {
        return $this->fields;
    }

    public function filterableBy(): array
    {
        return $this->fields;
    }

    public function searchableBy(): array
    {
        return $this->fields;
    }

    public function sortableBy(): array
    {
        return $this->fields;
    }

    public function aggregates(): array
    {
        return $this->fields;
    }
}
