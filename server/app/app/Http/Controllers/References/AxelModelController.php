<?php

namespace app\Http\Controllers\References;

use App\Models\AxleModel;
use Orion\Concerns\DisableAuthorization;
use Orion\Http\Controllers\Controller;

class AxelModelController extends Controller
{
    use DisableAuthorization;

    protected $model = AxleModel::class;

    protected array $fields = [
        'name',
        'description'
    ];
    public function exposedScopes(): array
    {
        return $this->fields;
    }

    public function filterableBy(): array
    {
        return $this->fields;
    }

    public function searchableBy(): array
    {
        return $this->fields;
    }

    public function sortableBy(): array
    {
        return $this->fields;
    }

    public function aggregates(): array
    {
        return $this->fields;
    }
}
