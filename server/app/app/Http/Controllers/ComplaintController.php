<?php

namespace App\Http\Controllers;

use App\Models\Complaint;
use App\Models\Machine;
use App\Policies\ComplaintPolicy;
use Illuminate\Database\Eloquent\Builder;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class ComplaintController extends Controller
{
    protected $model = Complaint::class;
    protected $policy = ComplaintPolicy::class;
    protected $fields = [
        'id',
        'operating_hours', // Наработка, м/час
        'failure_component_id', // Узел отказа
        'failure_description',// Описание отказа
        'repair_method_id', // Способ восстановления
        'used_spare_parts', // Используемые запасные части
        'failure_date',
        'recovery_date', // Дата восстановления
        'downtime_days', // Время простоя техники
        'machine_id', // Внешний ключ для машины, ссылается на таблицу машин
        'service_id', // Внешний ключ для сервисной компании, ссылается на таблицу пользователей
    ];
    public function includes() : array
    {
        return [
            'machine',
            'failureComponent',
            'repairMethod',
            'service',

        ];
    }
    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);

        $user = auth()->user();
        if ($user->isClient()) {
            $query->whereIn('machine_id',  Machine::query()->where('client_id', $user->client->id)->pluck('id'));
        }
        if ($user->isService()) {
            $query->where('service_id',  $user->service->id);
        }

        return $query;
    }
    public function exposedScopes(): array
    {
        return $this->fields;
    }

    public function filterableBy(): array
    {
        return $this->fields;
    }

    public function searchableBy(): array
    {
        return $this->fields;
    }

    public function sortableBy(): array
    {
        return $this->fields;
    }

    public function aggregates(): array
    {
        return $this->fields;
    }
}
