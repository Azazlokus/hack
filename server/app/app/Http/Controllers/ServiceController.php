<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Policies\ServicePolicy;
use Orion\Concerns\DisableAuthorization;
use Orion\Http\Controllers\Controller;

class ServiceController extends Controller
{
    use DisableAuthorization;
    protected $model = Service::class;
    //protected $policy = ServicePolicy::class;

    protected array $fields = [
        'name',
        'description'
    ];
    public function exposedScopes(): array
    {
        return $this->fields;
    }

    public function filterableBy(): array
    {
        return $this->fields;
    }

    public function searchableBy(): array
    {
        return $this->fields;
    }

    public function sortableBy(): array
    {
        return $this->fields;
    }

    public function aggregates(): array
    {
        return $this->fields;
    }
}
