<?php

namespace App\Http\Controllers;

use App\Http\Requests\MachineRequest;
use App\Models\Machine;
use App\Policies\MachinePolicy;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\User;


class MachineController extends Controller
{
    protected $model = Machine::class;
    protected string $police = MachinePolicy::class;
    protected $request = MachineRequest::class;

    protected array $fields = [
        'id',
        'machine_number', // Зав. № машины - уникальный номер машины
        'equipment_model_id', // Модель техники
        'engine_model_id', // Модель двигателя
        'engine_number', // Зав. № двигателя
        'transmission_model_id', // Модель трансмиссии
        'transmission_number', // Зав. № трансмиссии
        'axle_model_id',
        'axle_number', // Зав. № ведущего моста
        'steerable_axle_model_id', // Модель управляемого моста
        'steerable_axle_number', // Зав. № управляемого моста
        'delivery_agreement_number',
        'delivery_agreement_date',
        'shipping_date', // Дата отгрузки с завода
        'consignee', // Грузополучатель (конечный потребитель)
        'delivery_address', // Адрес поставки (эксплуатации)
        'equipment_configuration', // Комплектация (доп. опции)
        'client_id', // Клиент
        'service_id'
    ];

    /**
     * The relations that are allowed to be included together with a resource.
     *
     * @return array
     */
    public function includes(): array
    {
        return [
            '*',
            'equipmentModel',
            'engineModel',
            'users',
            'transmissionModel',
            'axleModel',
            'steerableAxleModel',
            'client',
            'service'
        ];
    }

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);
        $user = auth()->user();
        if (!$user) {
            return $query->select('machine_number', // Зав. № машины - уникальный номер машины
                'equipment_model_id', // Модель техники
                'engine_model_id', // Модель двигателя
                'engine_number', // Зав. № двигателя
                'transmission_model_id', // Модель трансмиссии
                'transmission_number', // Зав. № трансмиссии
                'axle_model_id',
                'axle_number', // Зав. № ведущего моста
                'steerable_axle_model_id', // Модель управляемого моста
                'steerable_axle_number'// Зав. № управляемого моста);
            );
        }
        if ($user->isClient()) {
            $query->where('client_id', $user->client->id);
        }
        if ($user->isService()) {
            $query->where('service_id', $user->service->id);
        }

        return $query;
    }

    public function exposedScopes(): array
    {
        return $this->fields;
    }

    public function filterableBy(): array
    {
        return $this->fields;
    }

    public function searchableBy(): array
    {
        return $this->fields;
    }

    /**
     * The attributes that are used for sorting.
     *
     * @return array
     */
    public function sortableBy(): array
    {
        return $this->fields;
    }

    public function aggregates(): array
    {
        return $this->fields;
    }
}
