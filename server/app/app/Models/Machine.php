<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Machine extends Model
{
    use HasFactory;

    protected $fillable = [
        'machine_number', // Зав. № машины - уникальный номер машины
        'equipment_model_id', // Модель техники
        'engine_model_id', // Модель двигателя
        'engine_number', // Зав. № двигателя
        'transmission_model_id', // Модель трансмиссии
        'transmission_number', // Зав. № трансмиссии
        'axle_model_id',
        'axle_number', // Зав. № ведущего моста
        'steerable_axle_model_id', // Модель управляемого моста
        'steerable_axle_number', // Зав. № управляемого моста
        'delivery_agreement_number',
        'delivery_agreement_date',
        'shipping_date', // Дата отгрузки с завода
        'consignee', // Грузополучатель (конечный потребитель)
        'delivery_address', // Адрес поставки (эксплуатации)
        'equipment_configuration', // Комплектация (доп. опции)
        'client_id', // Клиент
        'service_id'
    ];
    public function axleModel(): HasOne
    {
        return $this->hasOne(AxleModel::class, 'id', 'axle_model_id');
    }
    public function equipmentModel(): HasOne
    {
        return $this->hasOne(EquipmentModel::class, 'id', 'equipment_model_id');
    }
    public function engineModel(): HasOne
    {
        return $this->hasOne(EngineModel::class, 'id', 'engine_model_id');
    }
    public function transmissionModel(): HasOne
    {
        return $this->hasOne(TransmissionModel::class, 'id', 'transmission_model_id');
    }
    public function steerableAxleModel(): HasOne
    {
        return $this->hasOne(SteerableAxleModel::class, 'id', 'steerable_axle_model_id');
    }
    public function client(): HasOne
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }
    public function service(): HasOne
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
    public function technicalServices(): HasMany
    {
        return $this->hasMany(TechnicalService::class);
    }

    public function complaints(): HasMany
    {
        return $this->hasMany(Complaint::class);
    }
}
