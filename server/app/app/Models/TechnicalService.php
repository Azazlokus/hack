<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TechnicalService extends Model
{
    use HasFactory;

    public function machine(): BelongsTo
    {
        return $this->belongsTo(Machine::class);
    }

    protected $fillable = [
        'ts_type_id', // Вид ТО
        'ts_date', // Дата проведения ТО
        'operating_hours', // Наработка, м/час
        'work_order_number', // № заказ-наряда
        'work_order_date', // Дата заказ-наряда

        // Внешний ключ для машины, ссылается на таблицу машин
        'machine_id',
        'service_id'
    ];
    public function tsType(): HasOne
    {
        return $this->hasOne(TechnicalServiceType::class, 'id', 'ts_type_id');
    }
    public function service(): HasOne
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
}
