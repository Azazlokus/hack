<?php

namespace App\Models;

use App\Constants\RoleConstants;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToManyAlias;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $fillable = [
        'name',
        'login',
        'role',
        'description',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed',
    ];
    public function client(): HasOne
    {
        return $this->hasOne(Client::class);
    }
    public function service(): HasOne
    {
        return $this->hasOne(Service::class);
    }
    public static function boot(): void
    {
        parent::boot();

        static::created(function ($model) {
            if ($model->role === 'Клиент') {
                $client = new Client([
                    'user_id' => $model->id,
                    'name' => $model->name,
                    'description' => $model->description
                ]);
                $client->save();
            }
            if ($model->role === 'Сервисная организация') {
                $service = new Service([
                    'user_id' => $model->id,
                    'name' => $model->name,
                    'description' => $model->description
                ]);
                $service->save();
            }
        });
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }
    public function roles(): BelongsToManyAlias
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    public function hasRole($roleName) : bool
    {
        return $this->role == $roleName;
    }

    public function isService(): bool
    {
        return $this->hasRole(RoleConstants::COMPANY);
    }
    public function isAdmin(): bool
    {
        return $this->hasRole(RoleConstants::ADMIN);
    }
    public function isClient(): bool
    {
        return $this->hasRole(RoleConstants::CLIENT);
    }
    public function isManager(): bool
    {
        return $this->hasRole(RoleConstants::MANAGER);
    }
}
