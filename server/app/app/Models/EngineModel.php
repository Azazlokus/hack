<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class EngineModel extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable =[
      'name',
      'description'
    ];
    public function machine(): BelongsToMany
    {
        return $this->belongsToMany(Machine::class, 'id','id');
    }
}
