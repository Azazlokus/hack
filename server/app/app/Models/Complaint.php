<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Complaint extends Model
{
    use HasFactory;
    public function machine(): BelongsTo
    {
        return $this->belongsTo(Machine::class);
    }
    protected $fillable = [
        'operating_hours', // Наработка, м/час
        'failure_component_id', // Узел отказа
        'failure_description',// Описание отказа
        'repair_method_id', // Способ восстановления
        'used_spare_parts', // Используемые запасные части
        'failure_date',
        'recovery_date', // Дата восстановления
        'downtime_days', // Время простоя техники
        // Внешний ключ для машины, ссылается на таблицу машин
        'machine_id',
        // Внешний ключ для сервисной компании, ссылается на таблицу пользователей
        'service_id',
    ];
    protected $dates = [
        'failure_date',
        'recovery_date',
    ];
    public function failureComponent(): HasOne
    {
        return $this->hasOne(FailureComponent::class, 'id', 'failure_component_id');
    }
    public function repairMethod(): HasOne
    {
        return $this->hasOne(RepairMethod::class, 'id', 'repair_method_id');
    }
    public function service(): HasOne
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
    protected static function boot(): void
    {
        parent::boot();

        static::creating(function ($model) {
            if ($model->failure_date && $model->recovery_date) {
                $difference = Carbon::parse($model->recovery_date)->diffInDays(Carbon::parse($model->failure_date));
                $model->downtime_days = $difference;
            }
        });
    }
}
