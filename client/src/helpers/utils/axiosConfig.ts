import axios from 'axios';

const instance = axios.create({
    baseURL: __API_URL__,
});

const setAuthToken = (token: string) => {
    if (token) {
        instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        localStorage.setItem('access_token', token);
    } else {
        delete instance.defaults.headers.common['Authorization'];
        localStorage.removeItem('access_token');
    }
};

export { instance, setAuthToken };