/**
 * Соритровка строк в таблицах
 * @param a
 * @param b
 */
export const stringSort = (a:string| number,b:string| number) => {
  if(a?.toString().toLowerCase() < b?.toString().toLowerCase())  return -1;
  if(a?.toString().toLowerCase() > b?.toString().toLowerCase())  return 1;
  return 0;
}
/**
 * Соритровка в селектах
 * @param input
 * @param option
 */
export const filterOption = (input: string, option?: { label: string; value: string | number }) =>
  (option?.label ?? '').toLowerCase().includes(input.toLowerCase());