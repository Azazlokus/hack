import {setAuthToken} from "@/helpers/utils/axiosConfig";
import {NavigateFunction} from "react-router-dom";
import {jwtDecode} from "jwt-decode";
import {HookAPI} from "antd/es/modal/useModal";

export const checkTokenExpiration = (setUser: any, navigate: NavigateFunction,modal:HookAPI) => {
    const token = localStorage.getItem('access_token');
    if (token) {
        const tokenDecode = jwtDecode(token);
        const currentTime = Math.floor(Date.now() / 1000);
        const tokenExpiration = tokenDecode.exp;
        if (tokenExpiration-120 < currentTime) {
            setAuthToken(null);
            setUser(null);
            navigate('/login');
            modal.error({
                title: 'Ошибка',
                content: 'Время вашей сессии истекло. Пожалуйста, войдите снова.',
                okButtonProps: {
                  className: 'primary-red',
                }
            })
        }
    }
};