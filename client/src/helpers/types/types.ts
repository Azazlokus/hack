export type TRole = "Клиент" | "Менеджер" | "Администратор" | "Сервисная организация";
export type TEntity  = "machines" | "TO" | "complaint" | "users" | "references";
export type TReferences = "axle-models" | "engine-models" | "equipment-models" | "failure-components"  | "repair-methods" | "steerable-axle-models" | "technical-service-types" | "transmission-models" | "clients" | "services";
export type TIncludes = "machine" | "engineModel" | "axleModel" | "equipmentModel" | "failureComponent" | "repairMethod" | "steerableAxleModel" | "tsType" | "transmissionModel" | "client" | "service";