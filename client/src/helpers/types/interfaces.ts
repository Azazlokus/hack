import {TIncludes} from "@/helpers/types/types";

export interface IBackendFilters{
  type?: 'and' | 'or';
  field?: string;
  operator?:
    | '<'
    | '<='
    | '>'
    | '>='
    | '='
    | '!='
    | 'like'
    | 'not like'
    | 'ilike'
    | 'not ilike'
    | 'in'
    | 'not in'
    | 'all in'
    | 'any in';
  value?: string | number | (number | string)[];
  nested?: IBackendFilters[];
  isExternal?:boolean;
  label?:string
}

export interface IBackendSorts{
  field: string;
  direction: 'ascend' | 'descend';
}

export interface IBackendIncludes{
  relation : TIncludes
}
export interface UseEntityProps{
  enable?:boolean,
  filters?:IBackendFilters[],
  sort?:IBackendSorts[],
  page?:number,
  limit?:number,
  includes?:IBackendIncludes[]
}

export interface IMachines{
  id: number,
  machine_number: string; // Зав. № машины - уникальный номер машины
  equipment_model?:IReferences; // Модель техники
  equipment_model_id:number; // Модель техники
  engine_model?:IReferences; // Модель двигателя
  engine_model_id:number; // Модель двигателя
  engine_number:string; // Зав. № двигателя
  transmission_model?:IReferences; // Модель трансмиссии
  transmission_model_id:number; // Модель трансмиссии
  transmission_number:string; // Зав. № трансмиссии
  axle_model?:IReferences;
  axle_model_id:number;
  axle_number:string; // Зав. № ведущего моста
  steerable_axle_model?:IReferences; // Модель управляемого моста
  steerable_axle_model_id:number; // Модель управляемого моста
  steerable_axle_number:string; // Зав. № управляемого моста
  delivery_agreement_number: string,
  delivery_agreement_date: string,
  shipping_date:string; // Дата отгрузки с завода
  consignee:string; // Грузополучатель (конечный потребитель)
  delivery_address:string; // Адрес поставки (эксплуатации)
  equipment_configuration:string; // Комплектация (доп. опции)
  client?:IReferences; // Клиент
  client_id:number; // Клиент
  service?:IReferences; // Сервисная компания
  service_id:number; // Сервисная компания
  created_at: string,
  updated_at: string
}

export interface ITO{
  id: number,
  ts_type: IReferences,
  ts_type_id: number,
  ts_date: string,
  operating_hours: string,
  work_order_number: string,
  work_order_date: string,
  machine: IMachines,
  machine_id: number,
  service: IReferences,
  service_id: number,
  created_at: string,
  updated_at: string
}

export interface IComplaints{
  id: number,
  failure_date: string,
  operating_hours: string,
  failure_component: IReferences,
  failure_component_id: number,
  failure_description: string,
  repair_method: IReferences,
  repair_method_id: number,
  used_spare_parts: string,
  recovery_date: string,
  downtime_days: number,
  machine: IMachines,
  machine_id: number,
  service: IReferences,
  service_id: number,
  created_at: string,
  updated_at: string
}

export interface IUsers {
  id: number;
  name: string;
  login: string;
  role: string;
  description: string;
}

export interface IReferences {
  id: number;
  name: string;
  description: string;
}