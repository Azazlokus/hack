import {IReferences, ITO, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {TReferences} from "@/helpers/types/types";

interface UseReferencesProps extends UseEntityProps {
  model:TReferences,
  modelValue:string
}
export const useReferences = ({model,modelValue,enable,page,sort,limit,filters}:UseReferencesProps) => {
  const [references, setReferences] = useState<IReferences | null>(null);
  const [isReferencesFetching,setIsReferencesFetching] = useState(false);


  const getReferences = async () => {
    try{
      setIsReferencesFetching(true);
      const payload:Record<any,any> = {
        filters:[{
          field:'name',
          operator:'=',
          value:modelValue
        }]
      };
      sort && (payload['sort'] = sort);
      page && (payload['page'] = page);
      limit && (payload['limit'] = limit);
      filters?.length && (payload['filters']= filters);
      await axiosInstance.post<{data:IReferences[]}>(`/${model}/search`, payload)
        .then((res) => {
          setReferences(res.data.data[0] || null)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsReferencesFetching(false);
    }
  }

  const toInitialState = () => {
    setReferences(null)
    setIsReferencesFetching(false);
  }
  useEffect(()=>{
    enable && getReferences();
  },[enable,page,sort,limit,filters,modelValue])

  return {
    references,
    isReferencesFetching,
    toInitialState
  }
}