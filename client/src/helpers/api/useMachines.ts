import {IMachines, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";


export const useMachines = ({enable,page,sort,limit,filters,includes}:UseEntityProps) => {
  const [machines, setMachines] = useState<IMachines[]>([]);
  const [isMachinesFetching,setIsMachinesFetching] = useState(false);
  const getMachines = async () => {
    try{
      setIsMachinesFetching(true);
      const payload:Record<any,any> = {
        sort:[
          {field : "shipping_date", direction : "desc"},
        ]
      };
      page && (payload['page'] = page);
      limit && (payload['limit'] = limit);
      filters?.length && (payload['filters'] = filters);
      includes?.length && (payload['includes'] = includes);
      await axiosInstance.post<{data:IMachines[]}>(`/machines/search`, payload)
        .then((res) => {
          setMachines(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsMachinesFetching(false);
    }
  }

  const refetchMachines = () => {
    getMachines();
  }

  let getDebounce: ReturnType<typeof setTimeout>;

  useEffect(()=>{
    clearTimeout(getDebounce);
    getDebounce = setTimeout(()=>{
      enable &&getMachines();
    },500)

    return ()=>{
      clearTimeout(getDebounce);
    }
  },[enable,page,sort,limit,filters])

  return {
    machines,
    isMachinesFetching,
    refetchMachines
  }
}