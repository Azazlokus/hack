import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useTechnicalServiceTypes = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [technicalServiceTypes, setTechnicalServiceTypes] = useState<IReferences[]>([]);
  const [isTechnicalServiceTypesFetching,setIsTechnicalServiceTypesFetching] = useState(false);
  const getTechnicalServiceTypes = async () => {
    try{
      setIsTechnicalServiceTypesFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/technical-service-types/search`, payload)
        .then((res) => {
          setTechnicalServiceTypes(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsTechnicalServiceTypesFetching(false);
    }
  }

  const refetchTechnicalServiceTypes = () => {
    getTechnicalServiceTypes();
  }

  useEffect(()=>{
    enable && getTechnicalServiceTypes();
  },[enable,page,sort,limit,filters])

  return {
    technicalServiceTypes,
    isTechnicalServiceTypesFetching,
    refetchTechnicalServiceTypes
  }
}