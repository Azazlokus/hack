import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
interface UseServiceEntityProps extends UseEntityProps{
  withSelf?:boolean
}
export const useServices = ({enable,page,sort,limit,filters,withSelf}:UseServiceEntityProps) => {
  const [services, setServices] = useState<IReferences[]>([]);
  const [isServicesFetching,setIsServicesFetching] = useState(false);
  const getServices = async () => {
    try{
      setIsServicesFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/services/search`, payload)
        .then((res) => {
          setServices(res.data.data.filter((service) => service.name !== "Самостоятельно" || withSelf))
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsServicesFetching(false);
    }
  }

  useEffect(()=>{
    enable && getServices();
  },[enable,page,sort,limit,filters])

  return {
    services,
    isServicesFetching,
  }
}