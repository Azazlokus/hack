import {IComplaints, IMachines, ITO, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import axios from "axios";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";


export const useComplaints = ({enable,page,sort,limit,filters,includes}:UseEntityProps) => {
  const [complaints, setComplaints] = useState<IComplaints[]>([]);
  const [isComplaintsFetching,setIsComplaintsFetching] = useState(false);
  const getComplaints = async () => {
    try{
      setIsComplaintsFetching(true);
      const payload:Record<any,any> = {
        sort:[
          {field : "failure_date", direction : "desc"},
        ]
      };
      page && (payload['page'] = page);
      limit && (payload['limit'] = limit);
      filters?.length && (payload['filters'] = filters);
      includes?.length && (payload['includes'] = includes);
      await axiosInstance.post<{data:IComplaints[]}>(`/complaints/search`, payload)
        .then((res) => {
          setComplaints(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsComplaintsFetching(false);
    }
  }

  const refetchComplaints = () => {
    getComplaints();
  }

  let getDebounce: ReturnType<typeof setTimeout>
  useEffect(()=>{
    clearTimeout(getDebounce);
    getDebounce = setTimeout(()=>{
      enable && getComplaints();
    },500)

    return ()=>{
      clearTimeout(getDebounce);
    }
  },[enable,page,sort,limit,filters])

  return {
    complaints,
    isComplaintsFetching,
    refetchComplaints
  }
}