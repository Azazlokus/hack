import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useClients = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [clients, setClients] = useState<IReferences[]>([]);
  const [isClientsFetching,setIsClientsFetching] = useState(false);
  const getClients = async () => {
    try{
      setIsClientsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/clients/search`, payload)
        .then((res) => {
          setClients(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsClientsFetching(false);
    }
  }

  useEffect(()=>{
    enable && getClients();
  },[enable,page,sort,limit,filters])

  return {
    clients,
    isClientsFetching,
  }
}