import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useTransmissionModels = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [transmissionModels, setTransmissionModels] = useState<IReferences[]>([]);
  const [isTransmissionModelsFetching,setIsTransmissionModelsFetching] = useState(false);
  const getTransmissionModels = async () => {
    try{
      setIsTransmissionModelsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/transmission-models/search`, payload)
        .then((res) => {
          setTransmissionModels(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsTransmissionModelsFetching(false);
    }
  }

  const refetchTransmissionModels = () => {
    getTransmissionModels();
  }

  useEffect(()=>{
    enable && getTransmissionModels();
  },[enable,page,sort,limit,filters])

  return {
    transmissionModels,
    isTransmissionModelsFetching,
    refetchTransmissionModels
  }
}