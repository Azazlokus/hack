import {IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useEngineModels = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [engineModels, setEngineModels] = useState<IReferences[]>([]);
  const [isEngineModelsFetching,setIsEngineModelsFetching] = useState(false);
  const getEngineModels = async () => {
    try{
      setIsEngineModelsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/engine-models/search`, payload)
        .then((res) => {
          setEngineModels(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsEngineModelsFetching(false);
    }
  }

  const refetchEngineModels = () => {
    getEngineModels();
  }

  useEffect(()=>{
    enable && getEngineModels();
  },[enable,page,sort,limit,filters])

  return {
    engineModels,
    isEngineModelsFetching,
    refetchEngineModels
  }
}