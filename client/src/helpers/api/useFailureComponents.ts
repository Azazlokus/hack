import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useFailureComponents = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [failureComponents, setFailureComponents] = useState<IReferences[]>([]);
  const [isFailureComponentsFetching,setIsFailureComponentsFetching] = useState(false);
  const getFailureComponents = async () => {
    try{
      setIsFailureComponentsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/failure-components/search`, payload)
        .then((res) => {
          setFailureComponents(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsFailureComponentsFetching(false);
    }
  }

  const refetchFailureComponents = () => {
    getFailureComponents();
  }


  useEffect(()=>{
    enable && getFailureComponents();
  },[enable,page,sort,limit,filters])

  return {
    failureComponents,
    isFailureComponentsFetching,
    refetchFailureComponents
  }
}