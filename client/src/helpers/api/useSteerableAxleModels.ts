import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useSteerableAxleModels = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [steerableAxleModels, setSteerableAxleModels] = useState<IReferences[]>([]);
  const [isSteerableAxleModelsFetching,setIsSteerableAxleModelsFetching] = useState(false);
  const getSteerableAxleModels = async () => {
    try{
      setIsSteerableAxleModelsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/steerable-axle-models/search`, payload)
        .then((res) => {
          setSteerableAxleModels(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsSteerableAxleModelsFetching(false);
    }
  }

  const refetchSteerableAxleModels = () => {
    getSteerableAxleModels();
  }

  useEffect(()=>{
    enable && getSteerableAxleModels();
  },[enable,page,sort,limit,filters])

  return {
    steerableAxleModels,
    isSteerableAxleModelsFetching,
    refetchSteerableAxleModels
  }
}