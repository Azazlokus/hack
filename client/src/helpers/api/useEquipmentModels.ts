import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useEquipmentModels = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [equipmentModels, setEquipmentModels] = useState<IReferences[]>([]);
  const [isEquipmentModelsFetching,setIsEquipmentModelsFetching] = useState(false);
  const getEquipmentModels = async () => {
    try{
      setIsEquipmentModelsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/equipment-models/search`, payload)
        .then((res) => {
          setEquipmentModels(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsEquipmentModelsFetching(false);
    }
  }

  const refetchEquipmentModels = () => {
    getEquipmentModels();
  }

  useEffect(()=>{
    enable && getEquipmentModels();
  },[enable,page,sort,limit,filters])

  return {
    equipmentModels,
    isEquipmentModelsFetching,
    refetchEquipmentModels
  }
}