import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useAxleModels = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [axleModels, setAxleModels] = useState<IReferences[]>([]);
  const [isAxleModelsFetching,setIsAxleModelsFetching] = useState(false);
  const getAxleModels = async () => {
    try{
      setIsAxleModelsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/axle-models/search`, payload)
        .then((res) => {
          setAxleModels(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsAxleModelsFetching(false);
    }
  }

  const refetchAxleModels = () => {
    getAxleModels();
  }

  useEffect(()=>{
    enable && getAxleModels();
  },[enable,page,sort,limit,filters])

  return {
    axleModels,
    isAxleModelsFetching,
    refetchAxleModels
  }
}