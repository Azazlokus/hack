import { IReferences, UseEntityProps} from "@/helpers/types/interfaces";
import {useEffect, useState} from "react";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";

export const useRepairMethods = ({enable,page,sort,limit,filters}:UseEntityProps) => {
  const [repairMethods, setRepairMethods] = useState<IReferences[]>([]);
  const [isRepairMethodsFetching,setIsRepairMethodsFetching] = useState(false);
  const getRepairMethods = async () => {
    try{
      setIsRepairMethodsFetching(true);
      const payload:Record<any,any> = {};
      page && (payload['page'] = page);
      sort && (payload['sort'] = sort);
      limit && (payload['limit'] = limit);
      filters && (payload['filters'] = filters);
      await axiosInstance.post<{data:IReferences[]}>(`/repair-methods/search`, payload)
        .then((res) => {
          setRepairMethods(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsRepairMethodsFetching(false);
    }
  }

  const refetchRepairMethods = () => {
    getRepairMethods();
  }

  useEffect(()=>{
    enable && getRepairMethods();
  },[enable,page,sort,limit,filters])

  return {
    repairMethods,
    isRepairMethodsFetching,
    refetchRepairMethods
  }
}