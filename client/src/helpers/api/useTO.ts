import {IMachines, ITO, UseEntityProps} from "@/helpers/types/interfaces";
import {useCallback, useEffect, useState} from "react";
import axios from "axios";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";


export const useTO = ({enable,page,sort,limit,filters,includes}:UseEntityProps) => {
  const [TO, setTO] = useState<ITO[]>([]);
  const [isTOFetching,setIsTOFetching] = useState(false);

  const getTO = async () => {
    try{
      setIsTOFetching(true);
      const payload:Record<any,any> = {
        sort:[
          {field : "ts_date", direction : "desc"},
        ]
      };
      page && (payload['page'] = page);
      limit && (payload['limit'] = limit);
      filters.length && (payload['filters'] = filters);
      includes?.length && (payload['includes'] = includes);
      await axiosInstance.post<{data:ITO[]}>(`/technical-services/search`, payload)
        .then((res) => {
          setTO(res.data.data)
        })
    }
    catch(err){
      console.log(err)
    }
    finally{
      setIsTOFetching(false);
    }
  }

  const refetchTO = () => {
    getTO();
  }

  let getDebounce: ReturnType<typeof setTimeout>
  useEffect(()=>{
    clearTimeout(getDebounce);
    getDebounce = setTimeout(()=>{
      enable &&getTO();
    },500)

    return ()=>{
      clearTimeout(getDebounce);
    }
  },[enable,page,sort,limit,filters])

  return {
    TO,
    isTOFetching,
    refetchTO
  }
}