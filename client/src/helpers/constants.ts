import {TEntity, TReferences} from "@/helpers/types/types";

export const referenceType:Record<TReferences,string> = {
  "axle-models":"Модель ведущего моста",
  "engine-models":"Модель двигателя",
  "equipment-models":"Модель техники",
  "failure-components":"Узел отказа",
  "repair-methods":"Способ восстановления",
  "steerable-axle-models":"Модель управляемого моста",
  "technical-service-types":"Вид ТО",
  "transmission-models":"Модель трансмиссии",
  "clients":"Клиент",
  "services":"Сервисная организация"
}

export const entityName:Record<TEntity,string> = {
  complaint:"рекламацию",
  machines:"машину",
  TO:"ТО",
  users:"пользователя",
  references:"справочник"
}

export const referenceName:Record<TReferences,string> = {
    "axle-models":"Модель ведущего моста",
    "engine-models":"Модель двигателя",
    "equipment-models":"Модель техники",
    "failure-components":"Узел отказа",
    "repair-methods":"Способ восстановления",
    "steerable-axle-models":"Модель управляемого моста",
    "technical-service-types":"Вид ТО",
    "transmission-models":"Модель трансмиссии",
    "clients":"Клиент",
    "services":"Сервисная организация"
}