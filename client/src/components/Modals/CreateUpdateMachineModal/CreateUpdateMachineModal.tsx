import React, {useEffect} from 'react';
import {IMachines} from "@/helpers/types/interfaces";
import {Col, DatePicker, Form, Input, Modal, Row, Select} from "antd";
import {useEquipmentModels} from "@/helpers/api/useEquipmentModels";
import {useEngineModels} from "@/helpers/api/useEngineModels";
import {useTransmissionModels} from "@/helpers/api/useTransmissionModels";
import {useAxleModels} from "@/helpers/api/useAxleModels";
import {useSteerableAxleModels} from "@/helpers/api/useSteerableAxleModels";
import {useClients} from "@/helpers/api/useClients";
import {useServices} from "@/helpers/api/useServices";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import dayjs from 'dayjs';
import {filterOption} from "@/helpers/utils/scripts";
import {NotificationInstance} from "antd/es/notification/interface";


interface CreateUpdateMachineModalProps{
  machine:IMachines | null;
  isOpen:boolean;
  api:NotificationInstance;
  toggleModal:(needToRefetch?:boolean) => void;
}
const CreateUpdateMachineModal = ({machine,toggleModal,isOpen,api}:CreateUpdateMachineModalProps) => {
  const [isChangesSaving,setIsChangesSaving] = React.useState(false)
  const {isEquipmentModelsFetching,equipmentModels} = useEquipmentModels({
    enable:isOpen
  })
  const {isEngineModelsFetching,engineModels} = useEngineModels({
    enable:isOpen
  })
  const {isTransmissionModelsFetching,transmissionModels} = useTransmissionModels({
    enable:isOpen
  })
  const {isAxleModelsFetching,axleModels} = useAxleModels({
    enable:isOpen
  })
  const {isSteerableAxleModelsFetching,steerableAxleModels} = useSteerableAxleModels({
    enable:isOpen
  })
  const {clients,isClientsFetching} =useClients({
    enable:isOpen
  })
  const {services,isServicesFetching} =useServices({
    enable:isOpen
  })
  const [form] = Form.useForm();
  const dateFormat = "YYYY-MM-DD";
  const saveChanges = async () => {
      setIsChangesSaving(true);
      form.validateFields().then(async (values) => {
        const payload = {
          ...values,
          delivery_agreement_date:values.delivery_agreement_date.format(dateFormat),
          shipping_date:values.shipping_date.format(dateFormat),
        }
        if(machine){
          await axiosInstance.put(`/machines/${machine.id}`,payload)
            .then(() => {
              toggleModal(true);
              api.success({
                message: "Машина успешно изменена"
              })
            })
            .finally(() => setIsChangesSaving(false))
        }
        else{
          await axiosInstance.post(`/machines`,payload)
            .then(() => {
              toggleModal(true);
              api.success({
                message: "Машина успешно добавлена"
              })
            })
            .finally(() => setIsChangesSaving(false))
        }
      }).catch(() => setIsChangesSaving(false));
  }

  useEffect(() => {
    if(machine){
      form.setFieldsValue({
        ...machine,
        delivery_agreement_date:dayjs(machine.delivery_agreement_date,dateFormat),
        shipping_date:dayjs(machine.shipping_date,dateFormat),
      })
    }
  },[machine])

  return (
    <Modal
      title={machine ? "Редактирование ТО" : "Добавить ТО"}
      open={isOpen}
      onOk={saveChanges}
      okText={machine ? "Сохранить" : "Добавить"}
      cancelText="Отмена"
      onCancel={() => toggleModal()}
      destroyOnClose
      confirmLoading={isChangesSaving}
      closable
      width={1000}
    >
      <Form form={form} preserve={false} layout="vertical">
        <Row gutter={[16,4]}>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="machine_number"
              label="Зав. номер машины"
              rules={[
                {
                  required: true,
                  message: "Введите зав. номер машины"
                }
              ]}
            >
              <Input
                placeholder="Введите зав. номер машины"
                max={255}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="equipment_model_id"
              label="Модель техники"
              rules={[
                {
                  required: true,
                  message: "Выберите модель техники"
                }
              ]}
            >
              <Select
                placeholder="Выберите модель техники"
                options={equipmentModels.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isEquipmentModelsFetching}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="engine_model_id"
              label="Модель двигателя"
              rules={[
                {
                  required: true,
                  message: "Выберите модель двигателя"
                }
              ]}
            >
              <Select
                placeholder="Выберите модель двигателя"
                options={engineModels.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isEngineModelsFetching}
              />
            </Form.Item>
          </Col>

          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="engine_number"
              label="Зав. номер двигателя"
              rules={[
                {
                  required: true,
                  message: "Введите зав. номер двигателя"
                }]}
            >
              <Input
                placeholder="Введите зав. номер двигателя"
                max={255}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="transmission_model_id"
              label="Модель трансмиссии"
              rules={[
                {
                  required: true,
                  message: "Выберите модель трансмиссии"
                }]}
            >
              <Select
                placeholder="Выберите модель трансмиссии"
                options={transmissionModels.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isTransmissionModelsFetching}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="transmission_number"
              label="Зав. номер трансмиссии"
              rules={[
                {
                  required: true,
                  message: "Введите зав. номер трансмиссии"
                }]}
            >
              <Input
                placeholder="Введите зав. номер трансмиссии"
                max={255}
              />
            </Form.Item>
          </Col>

          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="axle_model_id"
              label="Модель ведущего моста"
              rules={[
                {
                  required: true,
                  message: "Выберите модель вед. моста"
                }]}
            >
              <Select
                placeholder="Выберите модель вед. моста"
                options={axleModels.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isAxleModelsFetching}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="axle_number"
              label="Зав. номер ведущего моста"
              rules={[
                {
                  required: true,
                  message: "Введите зав. номер ведущего моста"
                }]}
            >
              <Input
                placeholder="Введите зав. номер ведущего моста"
                max={255}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="steerable_axle_model_id"
              label="Модель управляемого моста"
              rules={[
                {
                  required: true,
                  message: "Выберите модель упр. моста"
                }]}
            >
              <Select
                placeholder="Выберите модель упр. моста"
                options={steerableAxleModels.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isSteerableAxleModelsFetching}
              />
            </Form.Item>
          </Col>

          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="steerable_axle_number"
              label="Зав. номер управляемого моста"
              rules={[
                {
                  required: true,
                  message: "Введите зав. номер упр. моста"
                }]}
            >
              <Input
                placeholder="Введите зав. номер упр. моста"
                max={255}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="delivery_agreement_number"
              label="Номер договора поставки"
              rules={[
                {
                  required: true,
                  message: "Введите номер договора поставки"
                }]}
            >
              <Input
                placeholder="Введите номер договора поставки"
                max={255}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="delivery_agreement_date"
              label="Дата договора поставки"
              rules={[
                {
                  required: true,
                  message: "Выберите дату договора поставки"
                }]}
            >
              <DatePicker
                style={{width:'100%'}}
                format={dateFormat}
                placeholder="Выберите дату договора поставки"
              />
            </Form.Item>
          </Col>

          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="shipping_date"
              label="Дата отгрузки с завода"
              rules={[
                {
                  required: true,
                  message: "Выберите дату отгрузки с завода"
                }]}
            >
              <DatePicker
                format={dateFormat}
                style={{width:'100%'}}
                placeholder="Выберите дату отгрузки с завода"
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="client_id"
              label="Клиент"
              rules={[
                {
                  required: true,
                  message: "Выберите клиента"
                }]}
            >
              <Select
                placeholder="Выберите клиента"
                options={clients.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isClientsFetching}
              />
            </Form.Item>
          </Col>
          <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="service_id"
              label="Сервисная организация"
              rules={[
                {
                  required: true,
                  message: "Выберите сервисную организацию"
                }]}
            >
              <Select
                placeholder="Выберите сервисную организацию"
                options={services.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isServicesFetching}
              />
            </Form.Item>
          </Col>


          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="delivery_address"
              label="Адрес поставки"
              rules={[
                {
                  required: true,
                  message: "Введите адрес поставки"
                }]}
            >
              <Input.TextArea
                placeholder="Введите адрес поставки"
                maxLength={255}
              />
            </Form.Item>
          </Col>

          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="consignee"
              label="Грузополучатель"
              rules={[
                {
                  required: true,
                  message: "Введите грузополучателя"
                }]}
            >
              <Input.TextArea
                placeholder="Введите грузополучателя"
                maxLength={255}
              />
            </Form.Item>
          </Col>

          <Col md={{span:24}} sm={{span:24}} xs={{span:24}}>
            <Form.Item
              name="equipment_configuration"
              label="Комплектация"
              rules={[
                {
                  required: true,
                  message: "Введите комплектация"
                }]}
            >
              <Input.TextArea
                placeholder="Введите комплектацию"
                maxLength={255}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default CreateUpdateMachineModal;