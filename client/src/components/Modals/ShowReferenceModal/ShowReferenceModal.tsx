import React from 'react';
import {Modal} from "antd";
import {TReferences} from "@/helpers/types/types";
import {referenceType} from "@/helpers/constants";
import classes from "@/components/Modals/style.module.scss";

interface ShowReferenceModalProps{
  isOpen:boolean;
  toggleModal: () => void;
  model:TReferences;
  modelValue:string;
  modelDescription:string;
}

const ShowReferenceModal = ({toggleModal,isOpen,model,modelValue,modelDescription}:ShowReferenceModalProps) => {
const closeModal = () => {
  toggleModal();
}
  return (
    <Modal
      title={`${referenceType[model]}: ${modelValue}`}
      open={isOpen}
      onOk={closeModal}
      onCancel={closeModal}
      closeIcon={true}
      cancelButtonProps={{
        disabled: true,
        hidden: true,
          className: classes['close-button']
      }}
      okText="Закрыть"
    >
       <p>{modelDescription ? modelDescription : "Нет дополнительных данных"}</p>
    </Modal>
  );
};

export default ShowReferenceModal;