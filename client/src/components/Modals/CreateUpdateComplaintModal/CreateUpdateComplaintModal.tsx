import React, {useEffect} from 'react';
import {IComplaints} from "@/helpers/types/interfaces";
import {Col, DatePicker, Form, Input, Modal, Row, Select} from "antd";
import {useTechnicalServiceTypes} from "@/helpers/api/useTechnicalServiceTypes";
import {useMachines} from "@/helpers/api/useMachines";
import {useServices} from "@/helpers/api/useServices";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {useFailureComponents} from "@/helpers/api/useFailureComponents";
import {useRepairMethods} from "@/helpers/api/useRepairMethods";
import {NotificationInstance} from "antd/es/notification/interface";
import dayjs from "dayjs";
import {filterOption} from "@/helpers/utils/scripts";

interface CreateUpdateComplaintModalProps{
  complaint:IComplaints | null;
  isOpen:boolean;
  toggleModal:(needToRefetch?:boolean) => void;
  api:NotificationInstance;
}
const CreateUpdateComplaintModal = ({toggleModal,isOpen,complaint,api}:CreateUpdateComplaintModalProps) => {
  const [isChangesSaving,setIsChangesSaving] = React.useState(false);

  const {isMachinesFetching,machines} = useMachines({
    enable:isOpen
  })
  const {isFailureComponentsFetching, failureComponents} = useFailureComponents({
    enable:isOpen
  })
  const {isRepairMethodsFetching, repairMethods} = useRepairMethods({
    enable:isOpen
  })
  const dateFormat = "YYYY-MM-DD";
  const [form] = Form.useForm()
  const saveChanges = async () => {
    setIsChangesSaving(true);
    form.validateFields()
      .then(async (values) => {
        const payload = {
          ...values,
          recovery_date:values.recovery_date.format("YYYY-MM-DD"),
          failure_date:values.failure_date.format("YYYY-MM-DD"),
          service_id:machines.find((machine) => machine.id === values.machine_id)?.service_id || null,
        }
        if(complaint){
          await axiosInstance.put(`/complaints/${complaint.id}`,payload)
            .then(() => {
              toggleModal(true);
              api.success({
                message: "Рекламация успешно обновлена",
              })
            })
            .finally(() => setIsChangesSaving(false))
        }
        else{
          await axiosInstance.post(`/complaints`,payload)
            .then(() => {
              toggleModal(true);
              api.success({
                message: "Рекламация успешно добавлена",
              })
            })
            .finally(() => setIsChangesSaving(false))
        }})
      .catch(() => setIsChangesSaving(false))
  }

  useEffect(() => {
    if(complaint){
      form.setFieldsValue({
        ...complaint,
        recovery_date:dayjs(complaint.recovery_date,dateFormat),
        failure_date:dayjs(complaint.failure_date,dateFormat),
      })
    }
  },[complaint])
  return (
    <Modal
      title={complaint ? "Редактирование рекламации" : "Добавить рекламацию"}
      open={isOpen}
      onOk={saveChanges}
      okText={complaint ? "Сохранить" : "Добавить"}
      cancelText="Отмена"
      onCancel={() => toggleModal()}
      destroyOnClose
      confirmLoading={isChangesSaving}
      closable
      width={800}
    >
      <Form form={form} preserve={false} layout="vertical">
        <Row gutter={[16,4]}>
          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="machine_id"
              label="Заводской номер машины"
              rules={[
                {
                  required: true,
                  message: "Выберите номер машины"
                }
              ]}
            >
              <Select
                placeholder="Выберите номер машины"
                options={machines.map(item => ({
                  value:item.id,
                  label:item.machine_number,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isMachinesFetching}
              />
            </Form.Item>
          </Col>
          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="failure_date"
              label="Дата отказа"
              rules={[
                {
                  required: true,
                  message: "Выберите дату отказа"
                }]}
            >
              <DatePicker
                style={{width:'100%'}}
                format="YYYY-MM-DD"
                placeholder="Выберите дату отказа"
              />
            </Form.Item>
          </Col>

          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="operating_hours"
              label="Наработка (м/час)"
              rules={[
                {
                  required: true,
                  message: "Введите наработку (м/час)"
                }]}
            >
              <Input
                placeholder="Введите наработку (м/час)"
                max={255}
              />
            </Form.Item>
          </Col>
          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="failure_component_id"
              label="Узел отказа"
              rules={[
                {
                  required: true,
                  message: "Выберите узел отказа"
                }
              ]}
            >
              <Select
                placeholder="Выберите узел отказа"
                options={failureComponents.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isFailureComponentsFetching}
              />
            </Form.Item>
          </Col>

          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="repair_method_id"
              label="Способ восстановления"
              rules={[
                {
                  required: true,
                  message: "Выберите cпособ восстановления"
                }
              ]}
            >
              <Select
                placeholder="Выберите cпособ восстановления"
                options={repairMethods.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isRepairMethodsFetching}
              />
            </Form.Item>
          </Col>
          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="recovery_date"
              label="Дата восстановления"
              rules={[
                {
                  required: true,
                  message: "Выберите дату восстановления"
                }]}
            >
              <DatePicker
                style={{width:'100%'}}
                format="YYYY-MM-DD"
                placeholder="Выберите дату отказа"
              />
            </Form.Item>
          </Col>

          <Col md={{span:24}} sm={{span:24}} xs={{span:24}}>
            <Form.Item
              name="used_spare_parts"
              label="Используемые запасные части"
              rules={[
                {
                  required: true,
                  message: "Введите используемые запасные части"
                }]}
            >
              <Input.TextArea
                placeholder="Введите используемые запасные части"
                maxLength={255}
              />
            </Form.Item>
          </Col>

          <Col md={{span:24}} sm={{span:24}} xs={{span:24}}>
            <Form.Item
              name="failure_description"
              label="Описание отказа"
              rules={[
                {
                  required: true,
                  message: "Введите описание отказа"
                }]}
            >
              <Input.TextArea
                placeholder="Введите описание отказа"
                maxLength={255}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default CreateUpdateComplaintModal;