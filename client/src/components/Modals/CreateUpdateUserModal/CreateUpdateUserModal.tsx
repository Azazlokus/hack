import React, {Dispatch, useEffect} from 'react';
import {Col, Form, Input, Modal, Radio, Row} from "antd";
import {IUsers} from "@/helpers/types/interfaces";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {NotificationInstance} from "antd/es/notification/interface";

interface CreateUpdateMachineModalProps{
    setUsers: Dispatch<React.SetStateAction<IUsers[]>>;
    users: IUsers[];
    isOpen:boolean;
    toggleModal:() => void;
    refetchUsers:() => void;
    user?:IUsers;
    api:NotificationInstance;
}

const CreateUpdateUserModel = ({isOpen, toggleModal, refetchUsers,user,api}: CreateUpdateMachineModalProps) => {
    const [isChangesSaving,setIsChangesSaving] = React.useState(false)

    const [form] = Form.useForm();

    const saveChanges = async () => {
        setIsChangesSaving(true);
        form.validateFields()
          .then(async (values) => {
              const payload = {
                  ...values
              }
              if(user){
                  await axiosInstance.put(`/users/${user.id}`,payload)
                    .then(() => {
                        toggleModal();
                        api.success({
                            message: "Пользователь успешно обновлен",
                        })
                        refetchUsers();
                    })
                    .finally(() => setIsChangesSaving(false))
              }
              else{
                  await axiosInstance.post(`/users`,payload)
                    .then(() => {
                        toggleModal();
                        api.success({
                            message: "Пользователь успешно добавлен",
                        })
                        refetchUsers();
                    })
                    .finally(() => setIsChangesSaving(false))
              }})
          .catch(() => setIsChangesSaving(false))
    };

    const roleOptions = [
        {
            value: 'Клиент',
            label: 'Клиент'
        },
        {
            value: 'Сервисная организация',
            label: 'Сервисная организация'
        },

        {
            value: 'Менеджер',
            label: 'Менеджер'
        },
        {
            value: 'Администратор',
            label: 'Администратор'
        }
    ]


    useEffect(() => {
        if(user){
            form.setFieldsValue({
                ...user
            })
        }
    },[user])
    return (
        <Modal
            title="Добавить пользователя"
            open={isOpen}
            okText="Добавить"
            onOk={saveChanges}
            cancelText="Отмена"
            onCancel={toggleModal}
            destroyOnClose
            confirmLoading={isChangesSaving}
            closable
            width={600}
        >
            <Form form={form} preserve={false} layout="vertical">
                <Row gutter={[16,4]}>
                    <Col md={{span:24}} sm={{span:24}} xs={{span:24}}>
                        <Form.Item
                          name="role"
                          label="Роль"
                          rules={[
                              {
                                  required: true,
                                  message: "Выберите роль пользователя"
                              }
                          ]}
                        >
                            <Radio.Group

                              options={roleOptions}
                              optionType="button"
                              buttonStyle="solid"
                            />

                        </Form.Item>
                    </Col>
                    <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
                        <Form.Item
                            name="name"
                            label="Имя"
                            rules={[
                                {
                                    required: true,
                                    message: "Введите имя пользователя"
                                }
                            ]}
                        >
                            <Input
                                placeholder="Введите имя пользователя"
                                max={255}
                            />
                        </Form.Item>
                    </Col>
                    <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
                        <Form.Item
                            name="login"
                            label="Логин"
                            rules={[
                                {
                                    required: true,
                                    message: "Введите логин пользователя"
                                }
                            ]}
                        >
                            <Input
                                placeholder="Введите логин пользователя"
                                max={255}
                            />
                        </Form.Item>
                    </Col>
                    <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
                        <Form.Item
                          name="password"
                          label="Пароль"
                          rules={[
                              {
                                  required: true,
                                  message: "Введите пароль пользователя"
                              }]}
                        >
                            <Input
                              type="password"
                              placeholder="Введите пароль пользователя"
                              max={255}
                            />
                        </Form.Item>
                    </Col>
                    <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
                        <Form.Item
                            name="description"
                            label="Описание"
                            rules={[
                                {
                                    required: false,
                                    message: "Введите описание пользователя"
                                }]}
                        >
                            <Input
                                placeholder="Введите описание пользователя"
                                max={255}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    );
};

export default CreateUpdateUserModel;