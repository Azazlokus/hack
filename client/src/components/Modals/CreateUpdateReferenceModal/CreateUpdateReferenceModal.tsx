import React, {useEffect} from 'react';
import {Col, Form, Input, Modal, notification, Row} from "antd";
import {TReferences} from "@/helpers/types/types";
import {referenceName} from "@/helpers/constants";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import TextArea from "antd/es/input/TextArea";
import classes from '@/components/Modals/style.module.scss';

type referenceEditProps = {
    isEdit:boolean;
    id:number,
    name:string | null,
    description:string | null,
}
interface CreateUpdateMachineModalProps {
    referenceActiveTab: TReferences;
    isOpen: boolean;
    toggleModal: (needToRefetch?:boolean) => void;
    setRefetchEntity:React.Dispatch<React.SetStateAction<boolean>>;
    setReferenceEdit:React.Dispatch<React.SetStateAction<referenceEditProps>>;
    referenceEdit:referenceEditProps;
}

type NotificationType = 'success' | 'error';
const CreateUpdateReferenceModal = ({
                                        referenceEdit,
                                        setReferenceEdit,
                                        setRefetchEntity,
                                        referenceActiveTab,
                                        toggleModal,
                                        isOpen
                                    }: CreateUpdateMachineModalProps) => {
    const [isChangesSaving, setIsChangesSaving] = React.useState(false)
    const [form] = Form.useForm();
    const [api, contextHolder] = notification.useNotification();
    const openNotificationSuccess = (type: NotificationType) => {
        switch (type) {
            case 'success':
                return api.success({
                    message: 'Успех',
                    description: `Запись успешно ${referenceEdit.isEdit ? 'изменена' : 'добавлена'}`,
                })
            case 'error':
                return api.error({
                    message: 'Ошибка',
                    description: `Произошла ошибка при ${referenceEdit.isEdit ? 'редактировании' : 'добавлении'} записи`,
                })
        }
    };

    const saveChanges = async () => {
        setIsChangesSaving(true);
        try {
            const values = await form.validateFields();
            const payload = {
                name: values.name,
                description: values.description,
            }
            if (referenceActiveTab && referenceEdit.isEdit === false) {
                await axiosInstance.post(`/${referenceActiveTab}`, payload)
                    .then(() => {
                        toggleModal(true);
                        openNotificationSuccess('success');
                        setRefetchEntity(true);
                    })
            } else {
                await axiosInstance.put(`/${referenceActiveTab}/${referenceEdit.id}`, payload)
                    .then(() => {
                        toggleModal(true);
                        openNotificationSuccess('success');
                        setRefetchEntity(true);
                        setReferenceEdit({
                            isEdit: false,
                            id: null,
                            name: null,
                            description: null,
                        })
                    })
            }
        } catch (error) {
            openNotificationSuccess('error');
        } finally {
            setIsChangesSaving(false);
        }
    }

    useEffect(() => {
        if(referenceEdit.isEdit){
            form.setFieldsValue({
                name:referenceEdit.name,
                description:referenceEdit.description,
            })
        }
    },[referenceEdit])

    const titleModal =
        referenceEdit.isEdit
            ?`Редактировать ${referenceName[referenceActiveTab].toLowerCase()}`
            : `Добавить ${referenceName[referenceActiveTab].toLowerCase()}`

    return (
        <>
            {contextHolder}
            <Modal
                className={classes.reference_modal}
                title={titleModal}
                open={isOpen}
                onOk={saveChanges}
                okText={referenceEdit.isEdit ? "Сохранить" : "Добавить"}
                cancelText="Отмена"
                onCancel={() => toggleModal()}
                destroyOnClose
                confirmLoading={isChangesSaving}
                closable
                width={500}
            >
                <Form className={classes.reference_form} form={form} preserve={false} layout="vertical">
                    <Row gutter={[16, 4]}>
                        <Col md={{span: 24}} sm={{span: 24}} xs={{span: 24}}>
                            <Form.Item
                                name="name"
                                label="Название"
                                rules={[
                                    {
                                        required: true,
                                        message: "Введите название"
                                    }
                                ]}
                            >
                                <Input
                                    placeholder="Введите название"
                                    max={255}
                                />
                            </Form.Item>
                        </Col>
                        <Col md={{span: 24}} sm={{span: 24}} xs={{span: 24}}>
                            <Form.Item
                                name="description"
                                label="Описание"
                                rules={[
                                    {
                                        required: false,
                                        message: "Введите описание"
                                    }
                                ]}
                            >
                                <TextArea
                                    size={"large"}
                                    placeholder="Введите описание"
                                    maxLength={255}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );

};

export default CreateUpdateReferenceModal;