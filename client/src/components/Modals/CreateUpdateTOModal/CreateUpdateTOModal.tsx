import React, {useEffect, useMemo} from 'react';
import {ITO} from "@/helpers/types/interfaces";
import {Col, DatePicker, Form, Input, Modal, Row, Select} from "antd";
import {useTechnicalServiceTypes} from "@/helpers/api/useTechnicalServiceTypes";
import {useMachines} from "@/helpers/api/useMachines";
import {useServices} from "@/helpers/api/useServices";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {NotificationInstance} from "antd/es/notification/interface";
import dayjs from "dayjs";
import {filterOption} from "@/helpers/utils/scripts";

interface CreateUpdateTOModalProps{
  TO:ITO | null;
  isOpen:boolean;
  api:NotificationInstance;
  toggleModal:(needToRefetch?:boolean) => void;
}
const CreateUpdateTOModal = ({toggleModal,isOpen,TO,api}:CreateUpdateTOModalProps) => {
  const [isChangesSaving,setIsChangesSaving] = React.useState(false);
  const {isTechnicalServiceTypesFetching,technicalServiceTypes} = useTechnicalServiceTypes({
    enable:isOpen
  })
  const {isMachinesFetching,machines} = useMachines({
    enable:isOpen,
    includes:[{relation:"service"}]
  })
  const {isServicesFetching,services} = useServices({
    enable:true,
    withSelf:true
  })
  const dateFormat = "YYYY-MM-DD";
  const [form] = Form.useForm();
  const selectedMachine =  Form.useWatch('machine_id',form);
  let machineService = machines.find((machine) => machine.id === selectedMachine)?.service || null;
  useEffect(() => {
    form.resetFields(['service_id']);
  },[machineService?.id]);
  const selfService = useMemo(() => {
    const item = services.find((service) => service.name === "Самостоятельно")
    return {
      value:item?.id,
      label:item?.name,
      key:item?.id
    }
  },[services]);
  const saveChanges = async () => {
    setIsChangesSaving(true);
    form.validateFields()
      .then(async (values) => {
        const payload = {
          ...values,
          ts_date:values.ts_date.format("YYYY-MM-DD"),
          work_order_date:values.work_order_date.format("YYYY-MM-DD"),
        }
        if(TO){
          await axiosInstance.put(`/technical-services/${TO.id}`,payload)
            .then(() => {
              toggleModal(true);
              api.success({
                message: "ТО успешно обновлено",
              })
            })
            .finally(() => setIsChangesSaving(false))
        }
        else{
          await axiosInstance.post(`/technical-services`,payload)
            .then(() => {
              toggleModal(true);
              api.success({
                message: "ТО успешно добавлено",
              })
            })
            .finally(() => setIsChangesSaving(false))
        }})
      .catch(() => setIsChangesSaving(false))
  }

  useEffect(() => {
    if(TO){
      form.setFieldsValue({
        ...TO,
        ts_date:dayjs(TO.ts_date,dateFormat),
        work_order_date:dayjs(TO.work_order_date,dateFormat),
        service_id:TO.service_id
      })
    }
  },[TO,machineService])

  return (
    <Modal
      title={TO ? "Редактирование ТО" : "Добавить ТО"}
      open={isOpen}
      onOk={saveChanges}
      okText={TO ? "Сохранить" : "Добавить"}
      cancelText="Отмена"
      onCancel={() => toggleModal()}
      destroyOnClose
      confirmLoading={isChangesSaving}
      closable
      width={800}
    >
      <Form form={form} preserve={false} layout="vertical">
        <Row gutter={[16,4]}>
          <Col md={{span:24}} sm={{span:24}} xs={{span:24}}>
            <Form.Item
              name="ts_type_id"
              label="Вид ТО"
              rules={[
                {
                  required: true,
                  message: "Выберите вид ТО"
                }
              ]}
            >
              <Select
                placeholder="Выберите вид ТО"
                options={technicalServiceTypes.map(item => ({
                  value:item.id,
                  label:item.name,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isTechnicalServiceTypesFetching}
              />
            </Form.Item>
          </Col>

          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="machine_id"
              label="Заводской номер машины"
              rules={[
                {
                  required: true,
                  message: "Выберите номер машины"
                }
              ]}
            >
              <Select
                placeholder="Выберите номер машины"
                options={machines.map(item => ({
                  value:item.id,
                  label:item.machine_number,
                  key:item.id
                }))}
                filterOption={filterOption}
                loading={isMachinesFetching}
              />
            </Form.Item>
          </Col>
          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="ts_date"
              label="Дата проведения ТО"
              rules={[
                {
                  required: true,
                  message: "Выберите дату проведения ТО"
                }]}
            >
              <DatePicker
                style={{width:'100%'}}
                format={dateFormat}
                placeholder="Выберите дату проведения ТО"
              />
            </Form.Item>
          </Col>

          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="operating_hours"
              label="Наработка (м/час)"
              rules={[
                {
                  required: true,
                  message: "Введите наработку (м/час)"
                }]}
            >
              <Input
                placeholder="Введите наработку (м/час)"
                max={255}
              />
            </Form.Item>
          </Col>
          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="work_order_number"
              label="№ заказ-наряда"
              rules={[
                {
                  required: true,
                  message: "Введите № заказ-наряда"
                }]}
            >
              <Input
                placeholder="Введите № заказ-наряда"
                max={255}
              />
            </Form.Item>
          </Col>

          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="work_order_date"
              label="Дата заказ-наряда"
              rules={[
                {
                  required: true,
                  message: "Выберите дату заказ-наряда"
                }]}
            >
              <DatePicker
                style={{width:'100%'}}
                format={dateFormat}
                placeholder="Выберите дату заказ-наряда"
              />
            </Form.Item>
          </Col>
          <Col md={{span:12}} sm={{span:12}} xs={{span:24}}>
            <Form.Item
              name="service_id"
              label="Организация проводившая ТО"
              rules={[
                {
                  required: true,
                  message: "Выберите организацию проводившую ТО"
                }
              ]}
            >
              <Select
                disabled={!machineService}
                placeholder="Выберите организацию проводившую ТО"
                options={[selfService,{value:machineService?.id,label:machineService?.name,key:machineService?.id}]}
                loading={isServicesFetching}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default CreateUpdateTOModal;