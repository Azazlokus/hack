import React, {useLayoutEffect, useRef} from 'react';
import classes from './style.module.scss';
import Tg from '@/assets/Tg.svg'
import Bg from './img/FooterBg.svg'
import {footerName, footerNumber} from '@/components/Footer/constants';
import gsap from "gsap";
import {ScrollTrigger} from "gsap/dist/ScrollTrigger";
import Container from "@/components/Container/Container";
gsap.registerPlugin(ScrollTrigger);
const Footer = () => {
    const footerRef = useRef<HTMLElement>(null);
    useLayoutEffect(() => {
        gsap.from(".footer_item", {
            opacity: 0,
            y: 80,
            duration:0.75,
            stagger: 0.1,
            scrollTrigger:footerRef.current,
        })
    },[])
    return (
        <footer className={classes.footer} ref={footerRef}>
            <Container>
                <div className={classes.footer_container}>
                    <Bg className={classes.footer_bg}/>
                    <div className={classes.footer_section + " footer_item"} >
                        <Tg className={classes.footer_sotial}/>
                        <span className={classes.footer_number}>{footerNumber}</span>
                    </div>
                    <span className={classes.footer_name + " footer_item"}>{footerName}</span>
                </div>
            </Container>
        </footer>
    );
};

export default Footer;