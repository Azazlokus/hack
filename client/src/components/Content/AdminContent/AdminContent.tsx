import React from 'react';
import classes from "@/components/Content/AdminContent/style.module.scss";
import AdminTableContent from "@/components/Content/AdminTableContent/AdminTableContent";

const AdminContent = () => {
    return (
        <section className={classes.content}>
            <AdminTableContent/>
        </section>
    );
};

export default AdminContent;