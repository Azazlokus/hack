import React, {useLayoutEffect, useRef} from 'react';
import classes from './style.module.scss';
import Bg from './img/ContentBg.webp';
import gsap from "gsap";
const HomeContent = () => {
  const imageRef = useRef<HTMLImageElement>(null);
  const headingRef = useRef<HTMLHeadingElement>(null);
  useLayoutEffect(() => {
    gsap.from(imageRef.current, {
      opacity: 0,
      y:500,
      duration:2,
      ease:"expo.out"
    })

    gsap.from(headingRef.current, {
      opacity: 0,
      x:-500,
      duration:2,
      ease:"expo.out"
    })
  },[])

  return (
        <section className={classes.content}>
            <img className={classes.content_car} src={Bg} alt="BackgroundCar" ref={imageRef}/>
            <h1 className={classes.content_text} ref={headingRef}>Электронная сервисная книжка «Мой Силант»</h1>
        </section>
    );
};

export default HomeContent;