import React from 'react';
import {Button, Space, Tabs, TabsProps} from "antd";
import {UserAddOutlined} from "@ant-design/icons";
import {TEntity} from "@/helpers/types/types";

interface TableTabsProps {
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
}

const AdminTableTabs = ({
                          setIsOpen
                        }: TableTabsProps) => {
    const itemsAdmin: TabsProps["items"] = [
        {
            label: `Пользователи`,
            key: 'users',
        }
    ]

    return (
        <Tabs
            activeKey={itemsAdmin[0].key}
            items={itemsAdmin}
            tabBarExtraContent={
                <Space size={8}>
                    <Button
                        onClick={() => setIsOpen(true)}
                        className='primary-gradient'
                        icon={<UserAddOutlined/>}
                    >
                        Добавить пользователя
                    </Button>
                </Space>
            }
        />
    );
};

export default AdminTableTabs;