import React, {useState} from 'react';
import classes from './style.module.scss';
import {useUser} from "@/app/store/UserContext";

import {TEntity, TReferences} from "@/helpers/types/types";
import UsersTable from "@/components/Content/AdminTableContent/AdminTables/UsersTable/UsersTable";
import AdminTableTabs from "@/components/Content/AdminTableContent/AdminTableTabs/AdminTableTabs";



const AdminTableContent = () => {
    const [isOpen, setIsOpen] = useState<boolean>(false);


    const {user} = useUser();

    const isAuthorized = !!user?.role;
    return (
        <section className={classes.content}>
            <h1 className={classes.content_text}>Пользователи системы </h1>
            {isAuthorized &&
                <>
                    <AdminTableTabs
                      setIsOpen={setIsOpen}
                    />

                    <UsersTable
                        isOpen={isOpen}
                        setIsOpen={setIsOpen}
                    />
                </>
            }
        </section>
    );
};

export default AdminTableContent;