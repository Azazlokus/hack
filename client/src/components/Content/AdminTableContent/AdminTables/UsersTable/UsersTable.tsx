import React, {useEffect, useState} from 'react';
import {Space, Table, Modal, Button, notification} from "antd";
import {useUser} from "@/app/store/UserContext";
import {ColumnsType} from "antd/es/table";
import {instance as axiosInstance, setAuthToken} from "@/helpers/utils/axiosConfig";
import FullscreenLoader from "@/ui/loader/FullscreenLoader";
import EmptyTable from "@/ui/table/EmptyTable";
import EmptyMachinesIcon from "@/assets/machines-icon.svg";
import {IUsers} from "@/helpers/types/interfaces";
import CreateUpdateUserModal from "@/components/Modals/CreateUpdateUserModal/CreateUpdateUserModal";
import {stringSort} from "@/helpers/utils/scripts";
import {CloseCircleFilled} from "@ant-design/icons";
import {AuthService} from "@/components/Content/AuthContent/AuthForm/api/authService";
import {useNavigate} from "react-router-dom";

const { confirm } = Modal;


const UsersTable = ({setIsOpen, isOpen}: any) => {
    const [users, setUsers] = useState([])
    const [isUsersFetching, setIsUsersFetching] = useState(false);
    const {user,setUser} = useUser();
    const isAuthorized = !!user?.role;
    const [api, contextHolder] = notification.useNotification();
    const navigate = useNavigate();
    const [editingUser, setEditingUser] = useState<IUsers | null>(null);

    useEffect(() => {
        setIsUsersFetching(true)
        axiosInstance.post('users/search',{limit:100})
            .then(r => {
                setIsUsersFetching(false)
                setUsers(r.data.data)
            })
            .catch(() => {

            })
    }, [])

    const deleteUser = async (deletingUser: IUsers) => {
        const adminCount = users.filter(user => user.role === "Администратор").length;
        const actorId = user?.user.id;

        if(adminCount === 1 && deletingUser.role === "Администратор") {
            confirm({
                title: 'Ошибка',
                icon: <CloseCircleFilled style={{color:"#D20A11"}}/>,
                content: `Невозможно удалить последнего администратора в системе`,
                okText: 'Продолжить',
                okType: 'danger',
                cancelButtonProps: {style: {display: 'none'}},
                onOk() {
                },
                onCancel() {},
            });
            return;
        }

        if (actorId === deletingUser.id) {
            confirm({
                title: 'Удалить?',
                icon: <CloseCircleFilled style={{color: "#D20A11"}}/>,
                content: `Вы дейтсвительно хотите удалить ваш аккаунт? Отменить данное действие будет невозможно.`,
                okText: 'Удалить',
                cancelText: 'Отмена',
                okType: 'danger',
                onOk() {
                    return new Promise<void>((resolve) => {
                        axiosInstance.delete(`users/${deletingUser.id}`).then(() => {
                            resolve();
                            logout()
                        })
                    }).catch(() => api.error({message: 'Что-то пошло не так. Пожалуйста, попробуйте еще раз.'}));
                },
                onCancel() {
                },
            })
            return;
        }
        confirm({
            title: 'Удалить?',
            icon: <CloseCircleFilled style={{color: "#D20A11"}}/>,
            content: `Вы дейтсвительно хотите удалить ${deletingUser.name}? Отменить данное действие будет невозможно.`,
            okText: 'Удалить',
            cancelText: 'Отмена',
            okType: 'danger',
            onOk() {
                return new Promise<void>((resolve) => {
                    axiosInstance.delete(`users/${deletingUser.id}`).then(() => {
                        resolve();
                        api.success({
                            message: `Пользователь ${deletingUser.name} успешно удален`,
                        });
                        refetchUsers();
                    })
                }).catch(() => api.error({message: 'Что-то пошло не так. Пожалуйста, попробуйте еще раз.'}));
            },
            onCancel() {
            },
        })
    };

    const refetchUsers = () => {
        setIsUsersFetching(true)
        axiosInstance.post('users/search',{limit:100})
            .then(r => {
                setUsers(r.data.data)
                setIsUsersFetching(false)
            })
            .catch(() => {})
    }

    const closeModal = () => {
        setEditingUser(null);
        setIsOpen(false);
    }

    const logout = () => {
        setUser(null);
        navigate('/login');
        AuthService.logout()
          .finally(() => {
              setAuthToken(null)
          })
    }

    const editUser = (user: IUsers) => {
        setEditingUser(user);
        setIsOpen(true);
    }
    const usersColumns: ColumnsType = [
        {
            title: 'Имя',
            dataIndex: 'name',
            key: 'name',
             sortDirections: ['ascend' , 'descend'],
            sorter: (a:any, b:any) => stringSort(a.name,b.name),
        },
        {
            title: 'Логин',
            dataIndex: 'login',
            key: 'login',
             sortDirections: ['ascend' , 'descend'],
            sorter: (a:any, b:any) => stringSort(a.login,b.login),
        },
        {
            title: 'Роль',
            dataIndex: 'role',
            key: 'role',
             sortDirections: ['ascend' , 'descend'],
            sorter: (a:any, b:any) => stringSort(a.role,b.role),
        },
        {
            title: 'Описание',
            dataIndex: 'description',
            key: 'description',
            render: (text: string) => text || 'Нет описания',
             sortDirections: ['ascend' , 'descend'],
            sorter: (a:any, b:any) => stringSort(a.description,b.description),
        },
        {
            title: 'Действия',
            dataIndex: 'actions',
            key: 'actions',
            render: (value, record: IUsers) => (
                <Space size="middle">
                    <Button type='link' onClick={() => deleteUser(record)}>Удалить</Button>
                    <Button type='link' onClick={() => editUser(record)}>Изменить</Button>
                </Space>
            ),
        },
    ];

    return (
        <>
            {isUsersFetching && <FullscreenLoader text="Загружаем данные о пользователях..."/>}
            {!isUsersFetching && !users.length &&
                <EmptyTable text="Нет данных о пользователях" icon={<EmptyMachinesIcon/>}/>}
            {!isUsersFetching && users.length > 0 && <Table
                columns={isAuthorized ? usersColumns : usersColumns.slice(0, 10)}
                dataSource={users}
                scroll={{x: 'max-content'}}
                rowKey="name"
                loading={isUsersFetching}
                pagination={{pageSize: 5}}
            >
            </Table>}
            <CreateUpdateUserModal api={api} user={editingUser} refetchUsers={refetchUsers} users={users} setUsers={setUsers} toggleModal={closeModal} isOpen={isOpen}/>
            {contextHolder}
        </>
    );
};

export default UsersTable;
