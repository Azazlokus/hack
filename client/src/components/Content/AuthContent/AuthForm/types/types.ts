export type TAuthResponse = {
    access_token: string,
    token_type: string,
    user_name: string,
    user_login: string,
    user_role: string,
    expires_in: number
}

export type TUser = {
    id: number,
    name: string,
    email: string,
    email_verified_at: string,
    updated_at: string,
    created_at: string
}

export type TUserData = {
    exp: number,
    iat: number,
    iss: string,
    jti: string,
    nbf: number,
    prv: string,
    role: string,
    sub: string,
    user: TUser
}

export type TAuthData = {
    login: string,
    password: string
}