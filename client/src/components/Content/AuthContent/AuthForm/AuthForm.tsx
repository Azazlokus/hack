import React, {useLayoutEffect, useRef, useState} from 'react';
import {Button, Form, FormInstance, Input, message} from 'antd';
import classes from './style.module.scss';
import { AuthService } from "@/components/Content/AuthContent/AuthForm/api/authService";
import {TAuthData, TUserData} from "@/components/Content/AuthContent/AuthForm/types/types";
import { instance, setAuthToken } from "@/helpers/utils/axiosConfig";
import { useNavigate } from "react-router-dom";
import {jwtDecode} from "jwt-decode";
import {useUser} from "@/app/store/UserContext";
import gsap from "gsap";

const AuthForm = () => {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const formRef = useRef<HTMLDivElement>(null);
    const {setUser} = useUser();

    const onFinish = (values: TAuthData) => {
        setLoading(true);
        AuthService.login(values.login, values.password)
            .then((response) => {
                setAuthToken(response.data.access_token);
                const decodedToken = jwtDecode(response.data.access_token);
                setUser(decodedToken as TUserData)
                instance.defaults.headers.common['Authorization'] = `Bearer ${response.data.access_token}`;
                navigate('/');
            })
            .catch((error) => {
                if (error.response && error.response.status === 401) {
                    form.setFields([
                        {
                            name: 'login',
                            errors: ['Пользователя с таким логином или паролем не существует'],
                        },
                        {
                            name: 'password',
                            errors: [''],
                        }
                    ]);
                } else {
                    message.error('Что-то пошло не так. Пожалуйста, попробуйте еще раз.');
                }
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useLayoutEffect(() => {
        gsap.from(formRef.current, {
            opacity: 0,
            x:300,
            duration:2,
            ease:"expo.out"
        });
    },[])
    return (
      <div ref={formRef} className={classes.form_wrapper}>
        <Form
            layout='vertical'
            className={classes.form}
            onFinish={onFinish}
            form={form}
        >
            <h1 className={classes.form_title}>Вход</h1>
            <Form.Item
                className={classes.form_item}
                label='Логин'
                name='login'
                rules={[
                    {
                        required: true,
                        message: 'Введите логин',
                    },
                ]}
            >
                <Input size="large" placeholder='Введите логин' max={255}/>
            </Form.Item>
            <Form.Item
                className={classes.form_item}
                label='Пароль'
                name='password'
                rules={[
                    {
                        required: true,
                        message: 'Введите пароль',
                    },
                ]}
            >
                <Input.Password size="large" placeholder='Введите пароль' max={255}/>
            </Form.Item>
            <Form.Item className={classes.form_item}>
                <Button
                    htmlType='submit'
                    className='primary-gradient'
                    loading={loading}
                    size="large"
                >
                    Войти
                </Button>
            </Form.Item>
        </Form>
      </div>
    );
};

export default AuthForm;
