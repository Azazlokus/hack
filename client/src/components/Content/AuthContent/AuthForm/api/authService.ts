import axios, {AxiosResponse} from "axios";
import {TAuthResponse, TUserData} from "@/components/Content/AuthContent/AuthForm/types/types";
import {instance as axiosInstance, setAuthToken} from '@/helpers/utils/axiosConfig';

export class AuthService {

    static async login(login: string, password: string): Promise<AxiosResponse<TAuthResponse>> {
        return await axios.post(`${__API_URL__}/auth/login`, {login, password});
    }

    static async logout(): Promise<void> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return axiosInstance.post('/auth/logout');
        } else {
            console.log('Токен не найден');
        }
    }

    static async me(): Promise<AxiosResponse<TUserData>> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.post(`/auth/me`, {});
        } else {
            throw new Error('Токен не найден');
        }

    }
}