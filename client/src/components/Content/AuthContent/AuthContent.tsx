import React, { useLayoutEffect, useRef} from 'react';
import classes from './style.module.scss';
import AuthCar from './img/AuthCar.webp'
import AuthForm from '@/components/Content/AuthContent/AuthForm/AuthForm';
import gsap from "gsap";

const AuthContent = () => {
  const imageRef = useRef<HTMLImageElement>(null);
  useLayoutEffect(() => {
    gsap.from(imageRef.current, {
      opacity: 0,
      y:500,
      duration:2,
      ease:"expo.out"
    });
  },[])
    return (
        <div className={classes.content}>
            <img className={classes.content_car} src={AuthCar} alt='AuthCar' ref={imageRef}/>
            <AuthForm />
        </div>
    );
};

export default AuthContent;