import React from 'react';
import {IBackendFilters} from "@/helpers/types/interfaces";
import {Button, Card, Col, Row, Select, Tag} from "antd";
import {DeleteOutlined} from "@ant-design/icons";
import {CustomTagProps} from "rc-select/lib/BaseSelect";
import {useFailureComponents} from "@/helpers/api/useFailureComponents";
import {useServices} from "@/helpers/api/useServices";
import {useRepairMethods} from "@/helpers/api/useRepairMethods";
import {useMachines} from "@/helpers/api/useMachines";
import {filterOption} from "@/helpers/utils/scripts";

interface ReclamationFiltersProps{
    filters:IBackendFilters[];
    externalFilters:IBackendFilters[];
    isFiltersOpen:boolean;
    setFilters:React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
    setExternalFilters: React.Dispatch<React.SetStateAction<IBackendFilters[]>>
}
const ComplaintFilters = ({isFiltersOpen, filters, setFilters, externalFilters, setExternalFilters}:ReclamationFiltersProps ) => {
    const {isFailureComponentsFetching, failureComponents} = useFailureComponents({
        enable:isFiltersOpen
    })
    const {isServicesFetching, services} = useServices({
        enable:isFiltersOpen
    })
    const {isRepairMethodsFetching, repairMethods} = useRepairMethods({
        enable:isFiltersOpen
    })
    const {isMachinesFetching,machines} = useMachines({
        enable:isFiltersOpen
    })
    const updateFilters = (key:string,value:string[]) => {
        if(externalFilters.find(item => item.field === key)) {
            setExternalFilters(prev => prev.filter(item => item.field !== key))
        }
        setFilters(prev => {
            if(!value.length) return [
                ...prev.filter((item) => item.field !== key),
            ]
            return [
                ...prev.filter((item) => item.field !== key),
                {field:key,operator:"in",value},
            ]
        })
    }

    const resetFilters = () =>{
        setFilters([])
        setExternalFilters([])
    }
    const tagRender = (props: CustomTagProps) => {
        const { label, closable, onClose } = props;
        const onPreventMouseDown = (event: React.MouseEvent<HTMLSpanElement>) => {
            event.preventDefault();
            event.stopPropagation();
        };
        return (
            <Tag
                onMouseDown={onPreventMouseDown}
                closable={closable}
                onClose={onClose}
                style={{ marginRight: 4 }}
                color={"red"}
            >
                {label}
            </Tag>
        );
    };
    if(!isFiltersOpen) return <></>
    return (
        <Card>
            <Row gutter={[16,16]} align="bottom">
                <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
                    <label htmlFor="machine_id" className="fake-antd-label">
                        Заводской номер машины
                    </label>
                    <Select
                      id="machine_id"
                      mode="multiple"
                      style={{ width: '100%' }}
                      maxTagCount="responsive"
                      placeholder="Выберите номер машины"
                      onChange={(value:string[]) => updateFilters("machine_id",value)}
                      allowClear={true}
                      loading={isMachinesFetching}
                      tagRender={tagRender}
                      filterOption={filterOption}
                      options={machines.map(item => ({
                          value:item.id,
                          label:item.machine_number,
                          key:item.id
                      }))}
                      value={filters.find(item => item.field === "machine_id")?.value}
                    />
                </Col>
                <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
                    <label htmlFor="failure_component_id" className="fake-antd-label">
                        Узел отказа
                    </label>
                    <Select
                        id="failure_component_id"
                        mode="multiple"
                        style={{ width: '100%' }}
                        maxTagCount="responsive"
                        placeholder="Выберите узел отказа"
                        onChange={(value:string[]) => updateFilters("failure_component_id",value)}
                        allowClear={true}
                        loading={isFailureComponentsFetching}
                        tagRender={tagRender}
                        filterOption={filterOption}
                        options={failureComponents.map(item => ({
                            value:item.id,
                            label:item.name,
                            key:item.id
                        }))}
                        value={filters.find(item => item.field === "failure_component_id")?.value}
                    />
                </Col>
                <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
                    <label htmlFor="repair_method_id" className="fake-antd-label">
                        Способ восстановления
                    </label>
                    <Select
                        id="repair_method_id"
                        mode="multiple"
                        style={{ width: '100%' }}
                        maxTagCount="responsive"
                        placeholder="Выберите способ восстановления"
                        onChange={(value:string[]) => updateFilters("repair_method_id",value)}
                        allowClear={true}
                        loading={isRepairMethodsFetching}
                        tagRender={tagRender}
                        filterOption={filterOption}
                        options={repairMethods.map(item => ({
                            value:item.id,
                            label:item.name,
                            key:item.id
                        }))}
                        value={filters.find(item => item.field === "repair_method_id")?.value}
                    />
                </Col>
                <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
                    <label htmlFor="service_id" className="fake-antd-label">
                        Сервисная компания
                    </label>
                    <Select
                        id="service_id"
                        mode="multiple"
                        style={{ width: '100%' }}
                        maxTagCount="responsive"
                        placeholder="Выберите сервисную компнаию"
                        onChange={(value:string[]) => updateFilters("service_id",value)}
                        allowClear={true}
                        loading={isServicesFetching}
                        tagRender={tagRender}
                        filterOption={filterOption}
                        options={services.map(item => ({
                            value:item.id,
                            label:item.name,
                            key:item.id
                        }))}
                        value={filters.find(item => item.field === "service_id")?.value}
                    />
                </Col>
                <Col md={{span:16}} sm={{span:12}} xs={{span:24}} style={{textAlign:'end'}}>
                    <Button
                        type="text"
                        onClick={resetFilters}
                        icon={<DeleteOutlined/>}
                        disabled={!filters.length }
                    >
                        Сбросить фильтры
                    </Button>
                </Col>
            </Row>
        </Card>
    );
};

export default ComplaintFilters;