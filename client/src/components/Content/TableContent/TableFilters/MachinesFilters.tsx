import React from 'react';
import {IBackendFilters} from "@/helpers/types/interfaces";
import {Button, Card, Col, Row, Select, Tag} from "antd";
import type { CustomTagProps } from 'rc-select/lib/BaseSelect';
import {useEquipmentModels} from "@/helpers/api/useEquipmentModels";
import {useEngineModels} from "@/helpers/api/useEngineModels";
import {useTransmissionModels} from "@/helpers/api/useTransmissionModels";
import {useAxleModels} from "@/helpers/api/useAxleModels";
import {useSteerableAxleModels} from "@/helpers/api/useSteerableAxleModels";
import {DeleteOutlined} from "@ant-design/icons";
import {filterOption} from "@/helpers/utils/scripts";

interface MachiesFiltersProps{
  filters:IBackendFilters[];
  externalFilters:IBackendFilters[];
  isFiltersOpen:boolean;
  setFilters:React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
}

const MachinesFilters = ({isFiltersOpen,filters,setFilters,externalFilters}:MachiesFiltersProps) => {
  const {isEquipmentModelsFetching,equipmentModels} = useEquipmentModels({
    enable:isFiltersOpen
  })
  const {isEngineModelsFetching,engineModels} = useEngineModels({
    enable:isFiltersOpen
  })
  const {isTransmissionModelsFetching,transmissionModels} = useTransmissionModels({
    enable:isFiltersOpen
  })
  const {isAxleModelsFetching,axleModels} = useAxleModels({
    enable:isFiltersOpen
  })
  const {isSteerableAxleModelsFetching,steerableAxleModels} = useSteerableAxleModels({
    enable:isFiltersOpen
  })
  const updateFilters = (key:string,value:string[]) => {
    setFilters(prev => {
      if(!value.length) return [
        ...prev.filter((item) => item.field !== key),
      ]
      return [
        ...prev.filter((item) => item.field !== key),
        {field:key,operator:"in",value},
      ]
    })
  }
  const tagRender = (props: CustomTagProps) => {
    const { label, closable, onClose } = props;
    const onPreventMouseDown = (event: React.MouseEvent<HTMLSpanElement>) => {
      event.preventDefault();
      event.stopPropagation();
    };
    return (
      <Tag
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{ marginRight: 4 }}
        color={"red"}
      >
        {label}
      </Tag>
    );
  };
  if(!isFiltersOpen) return <></>
   return (
    <Card>
      <Row gutter={[16,16]} align="bottom">
        <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
          <label htmlFor="equipment_model_id" className="fake-antd-label">
            Модель техники
          </label>
          <Select
            id="equipment_model_id"
            mode="multiple"
             style={{ width: '100%' }}
            maxTagCount="responsive"
            placeholder="Выберите модель техники"
            onChange={(value:string[]) => updateFilters("equipment_model_id",value)}
            allowClear={true}
            loading={isEquipmentModelsFetching}
            tagRender={tagRender}
            filterOption={filterOption}
            options={equipmentModels.map(item => ({
              value:item.id,
              label:item.name,
              key:item.id
            }))}
            value={filters.find(item => item.field === "equipment_model_id")?.value}
          />
        </Col>
        <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
          <label htmlFor="engine_model_id" className="fake-antd-label">
            Модель двигателя
          </label>
          <Select
            id="engine_model_id"
            mode="multiple"
             style={{ width: '100%' }}
            maxTagCount="responsive"
            placeholder="Выберите модель двигателя"
            onChange={(value:string[]) => updateFilters("engine_model_id",value)}
            allowClear={true}
            loading={isEngineModelsFetching}
            tagRender={tagRender}
            filterOption={filterOption}
            options={engineModels.map(item => ({
              value:item.id,
              label:item.name,
              key:item.id
            }))}
            value={filters.find(item => item.field === "engine_model_id")?.value}
          />
        </Col>
        <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
          <label htmlFor="transmission_model_id" className="fake-antd-label">
            Модель трансмиссии
          </label>
          <Select
            id="transmission_model_id"
            mode="multiple"
             style={{ width: '100%' }}
            maxTagCount="responsive"
            placeholder="Выберите модель трансмиссии"
            onChange={(value:string[]) => updateFilters("transmission_model_id",value)}
            allowClear={true}
            loading={isTransmissionModelsFetching}
            tagRender={tagRender}
            filterOption={filterOption}
            options={transmissionModels.map(item => ({
              value:item.id,
              label:item.name,
              key:item.id
            }))}
            value={filters.find(item => item.field === "transmission_model_id")?.value}
          />
        </Col>
        <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
          <label htmlFor="axle_model_id" className="fake-antd-label">
            Модель ведущего моста
          </label>
          <Select
            id="axle_model_id"
            mode="multiple"
             style={{ width: '100%' }}
            maxTagCount="responsive"
            placeholder="Выберите модель ведущего моста"
            onChange={(value:string[]) => updateFilters("axle_model_id",value)}
            allowClear={true}
            loading={isAxleModelsFetching}
            tagRender={tagRender}
            filterOption={filterOption}
            options={axleModels.map(item => ({
              value:item.id,
              label:item.name,
              key:item.id
            }))}
            value={filters.find(item => item.field === "axle_model_id")?.value}
          />
        </Col>
        <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
          <label htmlFor="steerable_axle_model_id" className="fake-antd-label">
            Модель управляемого моста
          </label>
          <Select
            id="steerable_axle_model_id"
            mode="multiple"
             style={{ width: '100%' }}
            maxTagCount="responsive"
            placeholder="Выберите модель управляемого моста"
            onChange={(value:string[]) => updateFilters("steerable_axle_model_id",value)}
            allowClear={true}
            loading={isSteerableAxleModelsFetching}
            tagRender={tagRender}
            filterOption={filterOption}
            options={steerableAxleModels.map(item => ({
              value:item.id,
              label:item.name,
              key:item.id
            }))}
            value={filters.find(item => item.field === "steerable_axle_model_id")?.value}
          />
        </Col>
        <Col md={{span:8}} sm={{span:12}} xs={{span:24}} style={{textAlign:'end'}}>
          <Button
            type="text"
            onClick={() => setFilters(prev => prev.filter(item => item.isExternal))}
            icon={<DeleteOutlined/>}
            disabled={filters.length === externalFilters.length}
          >
            Сбросить фильтры
          </Button>
        </Col>
      </Row>
    </Card>
  );
};

export default MachinesFilters;