import React from 'react';
import {IBackendFilters} from "@/helpers/types/interfaces";
import {Button, Card, Col, Row, Select, Tag} from "antd";
import type { CustomTagProps } from 'rc-select/lib/BaseSelect';
import {DeleteOutlined} from "@ant-design/icons";
import {useTechnicalServiceTypes} from "@/helpers/api/useTechnicalServiceTypes";
import {useMachines} from "@/helpers/api/useMachines";
import {useServices} from "@/helpers/api/useServices";
import {filterOption} from "@/helpers/utils/scripts";

interface ToFiltersProps {
    filters:IBackendFilters[];
    externalFilters:IBackendFilters[];
    isFiltersOpen:boolean;
    setFilters:React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
    setExternalFilters: React.Dispatch<React.SetStateAction<IBackendFilters[]>>
}

const ToFilters = ({isFiltersOpen,filters,setFilters,externalFilters,setExternalFilters}:ToFiltersProps) => {
    const {isTechnicalServiceTypesFetching,technicalServiceTypes} = useTechnicalServiceTypes({
        enable:isFiltersOpen
    })
    const {isMachinesFetching,machines} = useMachines({
        enable:isFiltersOpen
    })
    const {isServicesFetching, services} = useServices({
        enable:isFiltersOpen,
        withSelf:true
    })
    const updateFilters = (key:string,value:string[]) => {
        if(externalFilters.find(item => item.field === key)) {
            setExternalFilters(prev => prev.filter(item => item.field !== key))
        }
        setFilters(prev => {
            if(!value.length) return [
                ...prev.filter((item) => item.field !== key),
            ]
            return [
                ...prev.filter((item) => item.field !== key),
                {field:key,operator:"in",value},
            ]
        })
    }

    const resetFilters = () =>{
        setFilters([])
        setExternalFilters([])
    }

    const tagRender = (props: CustomTagProps) => {
        const { label, closable, onClose } = props;
        const onPreventMouseDown = (event: React.MouseEvent<HTMLSpanElement>) => {
            event.preventDefault();
            event.stopPropagation();
        };
        return (
            <Tag
                onMouseDown={onPreventMouseDown}
                closable={closable}
                onClose={onClose}
                style={{ marginRight: 4 }}
                color={"red"}
            >
                {label}
            </Tag>
        );
    };
    if(!isFiltersOpen) return <></>
    return (
        <Card>
            <Row gutter={[16,16]} align="bottom" justify="space-between">
                <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
                    <label htmlFor="machine_id" className="fake-antd-label">
                        Заводской номер машины
                    </label>
                    <Select
                      id="ts_type"
                      mode="multiple"
                      style={{ width: '100%' }}
                      maxTagCount="responsive"
                      placeholder="Выберите номер машины"
                      onChange={(value:string[]) => updateFilters("machine_id",value)}
                      allowClear={true}
                      loading={isMachinesFetching}
                      tagRender={tagRender}
                        filterOption={filterOption}
                        options={machines.map(item => ({
                          value:item.id,
                          label:item.machine_number,
                          key:item.id
                      }))}
                      value={filters.find(item => item.field === "machine_id")?.value}
                    />
                </Col>
                <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
                    <label htmlFor="ts_type_id" className="fake-antd-label">
                        Вид ТО
                    </label>
                    <Select
                        id="ts_type_id"
                        mode="multiple"
                        style={{ width: '100%' }}
                        maxTagCount="responsive"
                        placeholder="Выберите вид ТО"
                        onChange={(value:string[]) => updateFilters("ts_type_id",value)}
                        allowClear={true}
                        loading={isTechnicalServiceTypesFetching}
                        tagRender={tagRender}
                        filterOption={filterOption}
                        options={technicalServiceTypes.map(item => ({
                            value:item.id,
                            label:item.name,
                            key:item.id
                        }))}
                        value={filters.find(item => item.field === "ts_type_id")?.value}
                    />
                </Col>
                <Col md={{span:8}} sm={{span:12}} xs={{span:24}}>
                    <label htmlFor="service_id" className="fake-antd-label">
                        Организация проводившая ТО
                    </label>
                    <Select
                      id="service_id"
                      mode="multiple"
                      style={{ width: '100%' }}
                      maxTagCount="responsive"
                      placeholder="Выберите организацию"
                      onChange={(value:string[]) => updateFilters("service_id",value)}
                      allowClear={true}
                      loading={isServicesFetching}
                      tagRender={tagRender}
                        filterOption={filterOption}
                        options={services.map(item => ({
                          value:item.id,
                          label:item.name,
                          key:item.id
                      }))}
                      value={filters.find(item => item.field === "service_id")?.value}
                    />
                </Col>
                <Col md={{span:24}} sm={{span:12}} xs={{span:24}} style={{textAlign:'end'}}>
                    <Button
                        type="text"
                        onClick={resetFilters}
                        icon={<DeleteOutlined/>}
                        disabled={!filters.length}
                    >
                        Сбросить фильтры
                    </Button>
                </Col>
            </Row>
        </Card>
    );
};

export default ToFilters;