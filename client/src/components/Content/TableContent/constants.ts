export const buttonText = 'Поиск';
export const placeholderInput = 'Введите заводской номер';
export const mainContentText = 'Информация о комплектации и технические характеристики техники Силант';
export const secondaryContentText = 'Информация о проведенных ТО Вашей техники';
export const thirdContentText = 'Справочная информация';