import React, {useState} from 'react';
import classes from './style.module.scss';
import {mainContentText, secondaryContentText, thirdContentText} from '@/components/Content/TableContent/constants';
import MachinesTable from "@/components/Content/TableContent/Tables/MachinesTable/MachinesTable";
import TableTabs from "@/components/Content/TableContent/TableTabs/TableTabs";
import {useUser} from "@/app/store/UserContext";
import TOTable from "@/components/Content/TableContent/Tables/TOTable/TOTable";
import ComplaintsTable from "@/components/Content/TableContent/Tables/ComplaintTable/ComplaintsTable";
import {IBackendFilters, IComplaints, IMachines, ITO} from "@/helpers/types/interfaces";
import TableSearch from "@/components/Content/TableContent/TableSearch/TableSearch";
import {notification, Space, Tag} from "antd";
import ShowReferenceModal from "@/components/Modals/ShowReferenceModal/ShowReferenceModal";
import {TEntity, TReferences} from "@/helpers/types/types";
import CreateUpdateMachineModal from "@/components/Modals/CreateUpdateMachineModal/CreateUpdateMachineModal";
import CreateUpdateTOModal from "@/components/Modals/CreateUpdateTOModal/CreateUpdateTOModal";
import CreateUpdateComplaintModal from "@/components/Modals/CreateUpdateComplaintModal/CreateUpdateComplaintModal";
import ReferenceTabs from "@/components/Content/TableContent/TableTabs/ReferenceTabs";
import CreateUpdateReferenceModal from "@/components/Modals/CreateUpdateReferenceModal/CreateUpdateReferenceModal";
import gsap from "gsap";
import {ScrollTrigger} from "gsap/dist/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)
export interface IReferenceModel{
  model:TReferences | null,
  modelValue:string | null,
  modelDescription:string | null
}

export interface IEntityModel{
  machines:IMachines | null,
  TO:ITO | null,
  complaint:IComplaints | null,
}
 const TableContent = () => {
  const {user} = useUser();
  const isAuthorized = !!user?.role;
  const [api, contextHolder] = notification.useNotification();
  const [activeTab,setActiveTab] = useState<TEntity>("machines");
  const [referenceActiveTab,setReferenceActiveTab] = useState<TReferences>("engine-models");
  const [externalFilters,setExternalFilters] = useState<IBackendFilters[]>([]);
  const [isShowReferenceModal,setIsShowReferenceModal] = useState(false);
  const [referenceModel,setReferenceModel] = useState<IReferenceModel>({
    model:null,
    modelValue:null,
    modelDescription:null
  });
     const [referenceEdit, setReferenceEdit] = useState({
         isEdit: false,
         id: null,
         name: null,
         description: null
     });
  const [isShowEntityModal,setIsShowEntityModal] = useState<TEntity | TReferences | boolean>(false);
  const [entityModel,setEntityModel] = useState<IEntityModel>({
    machines:null,
    TO:null,
    complaint:null,
  });
  const [refetchEntity,setRefetchEntity] = useState(false);
  const [isFiltersOpen,setIsFiltersOpen] = useState(false);
  const showReference = (newReferenceModel:IReferenceModel) => {
    setReferenceModel(newReferenceModel);
    setIsShowReferenceModal(true)
  }
  const closeReferenceModal = () => {
    setIsShowReferenceModal(false)
  }

  const closeEntityModal = (needToRefetch = false) => {
    needToRefetch && setRefetchEntity(true);
    setIsShowEntityModal(false);
    setEntityModel({
      machines:null,
      TO:null,
      complaint:null
    });
      setReferenceEdit({
          isEdit: false,
          id: null,
          name: null,
          description: null
      })
  }

     const titleText = () => {
         switch (activeTab) {
             case "machines":
                 return mainContentText;
             case "TO":
                 return secondaryContentText;
             case "complaint":
                 return secondaryContentText;
             case "references":
                 return thirdContentText;
         }
     }
    return (
      <section className={classes.content} >
        <h1 className={classes.content_text}>{titleText()}</h1>
        {isAuthorized && externalFilters[0] && (
          <Space>
            <Tag color={"red"} closeIcon onClose={() => setExternalFilters([])}>
              Заводской номер: {externalFilters[0].label}
            </Tag>
          </Space>)}
        {isAuthorized &&
            <TableTabs
                referenceActiveTab={referenceActiveTab}
                activeTab={activeTab}
                setActiveTab={setActiveTab}
                setExternalFilters={setExternalFilters}
                isFiltersOpen={isFiltersOpen}
                setIsFiltersOpen={setIsFiltersOpen}
                setIsShowEntityModal={setIsShowEntityModal}
            />}
        {!isAuthorized && <TableSearch setExternalFilters={setExternalFilters}/>}

        {activeTab === "machines" &&
          <MachinesTable
            setExternalFilters={setExternalFilters}
            externalFilters={externalFilters}
            setActiveTab={setActiveTab}
            showReference={showReference}
            isFiltersOpen={isFiltersOpen}
            setRefetchEntity={setRefetchEntity}
            refetchEntity={refetchEntity}
            setEntityModel={setEntityModel}
            setIsShowEntityModal={setIsShowEntityModal}
          />
        }
        {activeTab === "TO" &&
          <TOTable
            setExternalFilters={setExternalFilters}
            externalFilters={externalFilters}
            showReference={showReference}
            isFiltersOpen={isFiltersOpen}
            setRefetchEntity={setRefetchEntity}
            refetchEntity={refetchEntity}
            setEntityModel={setEntityModel}
            setIsShowEntityModal={setIsShowEntityModal}
          />}
        {activeTab === "complaint" &&
          <ComplaintsTable
            setExternalFilters={setExternalFilters}
            externalFilters={externalFilters}
            showReference={showReference}
            isFiltersOpen={isFiltersOpen}
            setRefetchEntity={setRefetchEntity}
            refetchEntity={refetchEntity}
            setEntityModel={setEntityModel}
            setIsShowEntityModal={setIsShowEntityModal}
          />}
          {activeTab === "references" &&
              <ReferenceTabs
                  setIsShowEntityModal={setIsShowEntityModal}
                  setReferenceEdit={setReferenceEdit}
                  setRefetchEntity={setRefetchEntity}
                  refetchEntity={refetchEntity}
                  activeTab={referenceActiveTab}
                  setActiveTab={setReferenceActiveTab}
              />
          }
        <ShowReferenceModal
          isOpen={isShowReferenceModal}
          toggleModal={closeReferenceModal}
          model={referenceModel.model}
          modelValue={referenceModel.modelValue}
          modelDescription={referenceModel.modelDescription}
        />
        <CreateUpdateMachineModal
          machine={entityModel.machines}
          isOpen={isShowEntityModal === "machines"}
          toggleModal={closeEntityModal}
          api={api}
        />
        <CreateUpdateTOModal
          TO={entityModel.TO}
          isOpen={isShowEntityModal === "TO"}
          toggleModal={closeEntityModal}
          api={api}
        />
        <CreateUpdateComplaintModal
          complaint={entityModel.complaint}
          isOpen={isShowEntityModal === "complaint"}
          toggleModal={closeEntityModal}
          api={api}
        />
        <CreateUpdateReferenceModal
          referenceEdit={referenceEdit}
          setReferenceEdit={setReferenceEdit}
          setRefetchEntity={setRefetchEntity}
          referenceActiveTab={referenceActiveTab}
          isOpen={isShowEntityModal === "references"}
          toggleModal={closeEntityModal}
        />
        {contextHolder}
      </section>
    );
};

export default TableContent;