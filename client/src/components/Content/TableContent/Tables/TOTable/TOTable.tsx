import React, {useEffect, useState} from 'react';
import {Button, message, Modal, Space, Table} from "antd";
import {useUser} from "@/app/store/UserContext";
import {useTO} from "@/helpers/api/useTO";
import {ColumnsType} from "antd/es/table";
import {IBackendFilters, ITO} from "@/helpers/types/interfaces";
import {IEntityModel, IReferenceModel} from "@/components/Content/TableContent/TableContent";
import EmptyTable from "@/ui/table/EmptyTable";
import FullscreenLoader from "@/ui/loader/FullscreenLoader";
import EmptyTOIcon from "@/assets/to-icon.svg";
import ToFilters from "@/components/Content/TableContent/TableFilters/ToFilters";
import {TEntity} from "@/helpers/types/types";
import {CloseCircleFilled, DeleteFilled, EditFilled} from "@ant-design/icons";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {stringSort} from "@/helpers/utils/scripts";

interface ToTableProps{
  setExternalFilters: React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
  externalFilters:IBackendFilters[];
  showReference:(newReferenceModel:IReferenceModel) => void;
  isFiltersOpen:boolean;
  setRefetchEntity:React.Dispatch<React.SetStateAction<boolean>>;
  refetchEntity:boolean;
  setEntityModel:React.Dispatch<React.SetStateAction<IEntityModel>>;
  setIsShowEntityModal:React.Dispatch<React.SetStateAction<TEntity | boolean>>;
}

interface ITOColumns extends ITO{
  key: React.Key;
}

const { confirm } = Modal;

const ToTable = ({externalFilters,showReference, setExternalFilters, isFiltersOpen,refetchEntity,setRefetchEntity,setIsShowEntityModal,setEntityModel}:ToTableProps) => {
  const [page,setPage] = useState(1);
  const [filters,setFilters] = useState([]);
  const {user} = useUser();
  const isAuthorized = !!user?.role;
  const {isTOFetching,TO,refetchTO} = useTO(
      {
        enable:true,
        page,
        limit:100,
        filters,
        includes:[
          {relation:"service"},
          {relation:"machine"},
          {relation:"tsType"},
        ],
      }
  );
  const [messageApi, contextHolder] = message.useMessage();

  const showReferenceStop = (e:React.MouseEvent<HTMLElement, MouseEvent>,newReferenceModel:IReferenceModel) => {
    e.stopPropagation();
    showReference(newReferenceModel);
  }

  const TOColumns:ColumnsType<ITOColumns> = [
    {
      title: 'Зав. № машины',
      dataIndex: 'machine',
      key: 'machine',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.machine.machine_number,b.machine.machine_number),
      render: (value) => value.machine_number,
    },
    {
      title: 'Тип ТО',
      dataIndex: 'ts_type',
      key: 'ts_type',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.ts_type.name,b.ts_type.name),
      render: (value) => <Button type={"link"} onClick={(e)=> showReferenceStop(e,{
        model: "technical-service-types",
        modelValue: value.name,
        modelDescription: value.description
      })}>{value.name}</Button>
    },
    {
      title: 'Дата проведения ТО',
      dataIndex: 'ts_date',
      key: 'ts_date',
      sortDirections: ['ascend' , 'descend'],
      defaultSortOrder: 'descend',
      sorter: (a, b) => stringSort(a.ts_date,b.ts_date),
    },
    {
      title: 'Наработка, м/час',
      dataIndex: 'operating_hours',
      key: 'operating_hours',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.operating_hours,b.operating_hours),
    },
    {
      title: '№ заказ-наряда',
      dataIndex: 'work_order_number',
      key: 'work_order_number',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.work_order_number,b.work_order_number),
    },
    {
      title: 'Дата заказ-наряда',
      dataIndex: 'work_order_date',
      key: 'work_order_date',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.work_order_date,b.work_order_date),
    },
    {
      title: 'Организация проводившая ТО',
      dataIndex: 'service',
      key: 'service',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.service.name,b.service.name),
      render: (value) => <Button type={"link"} onClick={(e)=> showReferenceStop(e,{
        model: "services",
        modelValue: value.name,
        modelDescription: value.description
      })}>{value.name}</Button>,
    },
    {
      title: 'Действия',
      key: 'actions',
      fixed: 'right',
      width: 100,
      render: (_,record) => <Space size={4}>
        <Button type="text" icon={<EditFilled style={{color:"#163E6C"}} onClick={(e) => editTO(e,record)}/>}/>
        <Button type="text" icon={<DeleteFilled style={{color:"#163E6C"}} onClick={(e) => deleteTO(e,record)}/>}/>
      </Space>,
    }
  ]

  const editTO = (e:React.MouseEvent<HTMLElement, MouseEvent>,record:ITOColumns) => {
    e.stopPropagation();
    setEntityModel(prevState => ({
      ...prevState,
      TO:record,
    }));
    setIsShowEntityModal("TO");
  }

  const deleteTO = (e:React.MouseEvent<HTMLElement, MouseEvent>,record:ITOColumns) => {
    e.stopPropagation();
    confirm({
      title: 'Удалить ТО?',
      icon: <CloseCircleFilled style={{color:"#D20A11"}}/>,
      content: `Вы действительно хотите удалить ТО для машины ${record.machine.machine_number} от ${record.ts_date}?`,
      okText: 'Удалить',
      okType: 'danger',
      cancelText: 'Отмена',
      onOk() {
        return new Promise<void>((resolve) => {
          axiosInstance.delete(`technical-services/${record.id}`).then(() => {
            resolve();
            refetchTO();
          })
        }).catch(() => messageApi.error('Что-то пошло не так. Пожалуйста, попробуйте еще раз.'));
      },
      onCancel() {},
    });
  }

  useEffect(() => {
    setFilters(prev => [...prev.filter(filterItem => !filterItem.isExternal), ...externalFilters])
  },[externalFilters])

  useEffect(() => {
    if(refetchEntity){
      refetchTO()
      setRefetchEntity(false);
    }
  },[refetchEntity])
  return (
      <>
        <ToFilters
          filters={filters}
          isFiltersOpen={isFiltersOpen}
          setFilters={setFilters}
          externalFilters={externalFilters}
          setExternalFilters={setExternalFilters}
        />
        {isTOFetching && <FullscreenLoader text="Загружаем данные о проведенных ТО..."/>}
        {!isTOFetching && !TO.length && <EmptyTable text="Нет данных о проведенных ТО" icon={<EmptyTOIcon/>}/>}
        {!isTOFetching && TO.length > 0 && <Table
            columns={TOColumns}
            dataSource={TO}
            scroll={{ x: 'max-content' }}
            rowKey="id"
            pagination={{pageSize: 5}}
            loading={isTOFetching}
        />}
        {contextHolder}
      </>
  );
};

export default ToTable;

