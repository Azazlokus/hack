import React, {useEffect, useState} from 'react';
import {Button, message, Modal, Space, Table} from "antd";
import {useUser} from "@/app/store/UserContext";
import {ColumnsType} from "antd/es/table";
import {IBackendFilters, IComplaints} from "@/helpers/types/interfaces";
import {useComplaints} from "@/helpers/api/useComplaints";
import {IEntityModel, IReferenceModel} from "@/components/Content/TableContent/TableContent";
import EmptyTable from "@/ui/table/EmptyTable";
import FullscreenLoader from "@/ui/loader/FullscreenLoader";
import EmptyComplaintIcon from "@/assets/complaint-icon.svg";
import ComplaintFilters from "@/components/Content/TableContent/TableFilters/ComplaintFilters";
import {TEntity} from "@/helpers/types/types";
import {CloseCircleFilled, DeleteFilled, EditFilled} from "@ant-design/icons";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {stringSort} from "@/helpers/utils/scripts";

interface ComplaintsTableProps{
  setExternalFilters: React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
  externalFilters:IBackendFilters[];
  showReference:(newReferenceModel:IReferenceModel) => void;
  isFiltersOpen:boolean;
  setRefetchEntity:React.Dispatch<React.SetStateAction<boolean>>;
  refetchEntity:boolean;
  setEntityModel:React.Dispatch<React.SetStateAction<IEntityModel>>;
  setIsShowEntityModal:React.Dispatch<React.SetStateAction<TEntity | boolean>>;
}

interface IComplaintsTableColumns extends IComplaints{
  key: React.Key;
}
const { confirm } = Modal;

const ComplaintsTable = ({setExternalFilters,externalFilters,showReference,isFiltersOpen,refetchEntity,setRefetchEntity,setIsShowEntityModal,setEntityModel}:ComplaintsTableProps) => {
  const [page,setPage] = useState(1);
  const [filters,setFilters] = useState([]);
  const {complaints,isComplaintsFetching,refetchComplaints} = useComplaints({
    enable:true,
    page,
    limit:100,
    filters,
    includes:[
      {relation:"machine"},
      {relation : "failureComponent"},
      {relation : "repairMethod"},
      {relation : "service"}
    ]
  })
  const {user} = useUser();
  const canMutate = user?.role !== "Клиент" ;
  const [messageApi, contextHolder] = message.useMessage();


  const showReferenceStop = (e:React.MouseEvent<HTMLElement, MouseEvent>,newReferenceModel:IReferenceModel) => {
    e.stopPropagation();
    showReference(newReferenceModel);
  }

  const complaintsColumns:ColumnsType<IComplaintsTableColumns> = [
    {
      title: 'Зав. № машины',
      dataIndex: 'machine',
      key: 'machine',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.machine.machine_number,b.machine.machine_number),
      render: (value) => value.machine_number
    },
    {
      title:"Дата отказа",
      dataIndex:"failure_date",
      key:"failure_date",
      sortDirections: ['ascend' , 'descend'],
      defaultSortOrder: 'descend',
      sorter: (a, b) => stringSort(a.failure_date,b.failure_date),
    },
    {
      title:"Наработка, м/час",
      dataIndex:"operating_hours",
      key:"operating_hours",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.operating_hours,b.operating_hours),
    },
    {
      title:"Узел отказа",
      dataIndex:"failure_component",
      key:"failure_component",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.failure_component.name,b.failure_component.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "failure-components",
        modelValue: value.name,
        modelDescription: value.description
      })}>{value.name}</Button>,
    },
    {
      title:"Описание отказа",
      dataIndex:"failure_description",
      key:"failure_description",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.failure_description,b.failure_description),
    },
    {
      title:"Способ восстановления",
      dataIndex:"repair_method",
      key:"repair_method",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.repair_method.name,b.repair_method.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "repair-methods",
        modelValue: value.name,
        modelDescription: value.description
      })}>{value.name}</Button>,
    },
    {
      title:"Используемые зап.части",
      dataIndex:"used_spare_parts",
      key:"used_spare_parts",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.used_spare_parts,b.used_spare_parts),
    },
    {
      title:"Дата восстановления",
      dataIndex:"recovery_date",
      key:"recovery_date",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.recovery_date,b.recovery_date),
    },
    {
      title:"Время простоя техники",
      dataIndex:"downtime_days",
      key:"downtime_days",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.downtime_days,b.downtime_days),
    },
    {
      title:"Сервисная компания",
      dataIndex:"service",
      key:"service",
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.service.name,b.service.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "services",
        modelValue: value.name,
        modelDescription: value.description
      })}>{value.name}</Button>,
    },
    {
      title: 'Действия',
      key: 'actions',
      fixed: 'right',
      width: 100,
      render: (_,record) => <Space size={4}>
        <Button type="text" icon={<EditFilled style={{color:"#163E6C"}} onClick={(e) => editComplaint(e,record)}/>}/>
        <Button type="text" icon={<DeleteFilled style={{color:"#163E6C"}} onClick={(e) => deleteComplaint(e,record)}/>}/>
      </Space>,
    }
  ]

  const getComplaintsColumns = (columns:ColumnsType<IComplaintsTableColumns>) => {
    !canMutate && columns.pop();
    return columns;
  }

  const editComplaint = (e:React.MouseEvent<HTMLElement, MouseEvent>,record:IComplaintsTableColumns) => {
    e.stopPropagation();
    setEntityModel(prevState => ({
      ...prevState,
      complaint:record,
    }));
    setIsShowEntityModal("complaint");
  }

  const deleteComplaint = (e:React.MouseEvent<HTMLElement, MouseEvent>,record:IComplaintsTableColumns) => {
    e.stopPropagation();
    confirm({
      title: 'Удалить рекламацию?',
      icon: <CloseCircleFilled style={{color:"#D20A11"}}/>,
      content: `Вы действительно хотите удалить рекламацию для машины ${record.machine.machine_number} от ${record.failure_date}?`,
      okText: 'Удалить',
      okType: 'danger',
      cancelText: 'Отмена',
      onOk() {
        return new Promise<void>((resolve) => {
          axiosInstance.delete(`complaints/${record.id}`).then(() => {
            resolve();
            refetchComplaints();
          })
        }).catch(() => messageApi.error('Что-то пошло не так. Пожалуйста, попробуйте еще раз.'));
      },
      onCancel() {},
    });
  }

  useEffect(() => {
    setFilters(prev => [...prev.filter(filterItem => !filterItem.isExternal), ...externalFilters])
  },[externalFilters])

  useEffect(() => {
    if(refetchEntity){
      refetchComplaints();
      setRefetchEntity(false);
    }
  },[refetchEntity])
  return (
      <>
        <ComplaintFilters
          filters={filters}
          isFiltersOpen={isFiltersOpen}
          setFilters={setFilters}
          externalFilters={externalFilters}
          setExternalFilters={setExternalFilters}
        />
        {isComplaintsFetching && <FullscreenLoader text="Загружаем данные о рекламациях..."/>}
        {!isComplaintsFetching && !complaints.length && <EmptyTable text="Нет данных о рекламациях" icon={<EmptyComplaintIcon/>}/>}
        {!isComplaintsFetching && complaints.length > 0 && <Table
            columns={getComplaintsColumns(complaintsColumns)}
            dataSource={complaints}
            scroll={{ x: 'max-content' }}
            rowKey="id"
            pagination={{pageSize: 5}}
            loading={isComplaintsFetching}
        />}
        {contextHolder}
      </>
  );
};

export default ComplaintsTable;

