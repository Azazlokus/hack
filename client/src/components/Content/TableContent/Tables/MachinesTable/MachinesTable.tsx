import React, {useEffect, useMemo, useState} from 'react';
import {useMachines} from "@/helpers/api/useMachines";
import {Button, Input, message, Modal, Space, Table} from "antd";
import {ColumnsType} from "antd/es/table";
import {useUser} from "@/app/store/UserContext";
import {IBackendFilters, IMachines, IReferences} from "@/helpers/types/interfaces";
import {IEntityModel, IReferenceModel} from "@/components/Content/TableContent/TableContent";
import EmptyMachinesIcon from '@/assets/machines-icon.svg'
import EmptyTable from "@/ui/table/EmptyTable";
import FullscreenLoader from "@/ui/loader/FullscreenLoader";
import MachinesFilters from "@/components/Content/TableContent/TableFilters/MachinesFilters";
import {CloseCircleFilled, DeleteFilled, EditFilled, ExclamationCircleFilled} from "@ant-design/icons";
import {TEntity} from "@/helpers/types/types";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {stringSort} from "@/helpers/utils/scripts";

interface MachinesTableProps{
  setExternalFilters: React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
  externalFilters:IBackendFilters[];
  setActiveTab:React.Dispatch<React.SetStateAction<"machines" | "TO" | "complaint">>;
  showReference:(newReferenceModel:IReferenceModel) => void;
  isFiltersOpen:boolean;
  setRefetchEntity:React.Dispatch<React.SetStateAction<boolean>>;
  refetchEntity:boolean;
  setEntityModel:React.Dispatch<React.SetStateAction<IEntityModel>>;
  setIsShowEntityModal:React.Dispatch<React.SetStateAction<TEntity | boolean>>;
}
interface IMachinesColumns extends IMachines{
  key: React.Key;
}
const { confirm } = Modal;
const MachinesTable = ({setExternalFilters,externalFilters,setActiveTab,showReference,isFiltersOpen,refetchEntity,setRefetchEntity,setIsShowEntityModal,setEntityModel}:MachinesTableProps) => {
  const [page,setPage] = useState(1);
  const [filters,setFilters] = useState([]);
  const [messageApi, contextHolder] = message.useMessage();
  const {user} = useUser();
  const isAuthorized = !!user?.role;
  const canMutate = user?.role === "Администратор" || user?.role === "Менеджер";
  const {machines,isMachinesFetching , refetchMachines} = useMachines(
    {
      enable:isAuthorized || !!filters.length ,
      page,
      limit:100,
      filters,
      includes:[
        {relation:"equipmentModel"},
        {relation:"engineModel"},
        {relation:"transmissionModel"},
        {relation:"axleModel"},
        {relation:"steerableAxleModel"},
        {relation:"client"},
        {relation:"service"}
      ]
    }
  );

  const showReferenceStop = (e:React.MouseEvent<HTMLElement, MouseEvent>,newReferenceModel:IReferenceModel) => {
    e.stopPropagation();
    showReference(newReferenceModel);
  }

  const machinesColumns:ColumnsType<IMachinesColumns> = [
    {
      title: 'Зав. № машины',
      dataIndex: 'machine_number',
      key: 'machine_number',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.machine_number,b.machine_number),
    },
    {
      title: 'Модель техники',
      dataIndex: 'equipment_model',
      key: 'equipment_model',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.equipment_model.name,b.equipment_model.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "equipment-models",
        modelValue: value.name,
        modelDescription: value.description
      })}>{value.name}</Button>,
    },
    {
      title: 'Модель двигателя',
      dataIndex: 'engine_model',
      key: 'engine_model',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.engine_model.name,b.engine_model.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "engine-models",
        modelValue: value.name,
        modelDescription: value.description

      })}>{value.name}</Button>,
    },
    {
      title: 'Зав. № двигателя',
      dataIndex: 'engine_number',
      key: 'engine_number',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.engine_number,b.engine_number),
    },
    {
      title: 'Модель трансмиссии',
      dataIndex: 'transmission_model',
      key: 'transmission_model',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.transmission_model.name,b.transmission_model.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "transmission-models",
        modelValue: value.name,
        modelDescription: value.description

      })}>{value.name}</Button>,
    },
    {
      title: 'Зав. № трансмиссии',
      dataIndex: 'transmission_number',
      key: 'transmission_number',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.transmission_number,b.transmission_number),
    },
    {
      title: 'Модель ведущего моста',
      dataIndex: 'axle_model',
      key: 'axle_model',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.axle_model.name,b.axle_model.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "axle-models",
        modelValue: value.name,
        modelDescription: value.description

      })}>{value.name}</Button>,
    },
    {
      title: 'Зав. № ведущего моста',
      dataIndex: 'axle_number',
      key: 'axle_number',
      sorter: (a, b) => stringSort(a.axle_number,b.axle_number),
      sortDirections: ['ascend' , 'descend'],
    },
    {
      title: 'Модель управляемого моста',
      dataIndex: 'steerable_axle_model',
      key: 'steerable_axle_model',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.steerable_axle_model.name,b.steerable_axle_model.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "steerable-axle-models",
        modelValue: value.name,
        modelDescription: value.description

      })}>{value.name}</Button>,
    },
    {
      title: 'Зав. № управляемого моста',
      dataIndex: 'steerable_axle_number',
      key: 'steerable_axle_number',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.steerable_axle_number,b.steerable_axle_number),
    },
    {
      title: 'Номер договора поставки',
      dataIndex: 'delivery_agreement_number',
      key: 'delivery_agreement_number',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.delivery_agreement_number,b.delivery_agreement_number),
    },
    {
      title: 'Дата поставки',
      dataIndex: 'delivery_agreement_date',
      key: 'delivery_agreement_date',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.delivery_agreement_date,b.delivery_agreement_date),
    },
    {
      title: 'Дата отгрузки с завода',
      dataIndex: 'shipping_date',
      key: 'shipping_date',
      sortDirections: ['ascend' , 'descend'],
      defaultSortOrder: 'descend',
      sorter: (a, b) => stringSort(a.shipping_date,b.shipping_date),
    },
    {
      title: 'Грузополучатель',
      dataIndex: 'consignee',
      key: 'consignee',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.consignee,b.consignee),
    },
    {
      title: 'Адрес поставки',
      dataIndex: 'delivery_address',
      key: 'delivery_address',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.delivery_address,b.delivery_address),
    },
    {
      title: 'Комплектация',
      dataIndex: 'equipment_configuration',
      key: 'equipment_configuration',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.equipment_configuration,b.equipment_configuration),
    },
    {
      title: 'Клиент',
      dataIndex: 'client',
      key: 'client',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.client.name,b.client.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "clients",
        modelValue: value.name,
        modelDescription: value.description

      })}>{value.name}</Button>,
    },
    {
      title: 'Сервисная компания',
      key: 'service',
      sortDirections: ['ascend' , 'descend'],
      sorter: (a, b) => stringSort(a.service.name,b.service.name),
      render: (value) => <Button type={"link"} onClick={(e) => showReferenceStop(e,{
        model: "services",
        modelValue: value.name,
        modelDescription: value.description

      })}>{value.name}</Button>,
      dataIndex: 'service',
    },
    {
      title: 'Действия',
      key: 'actions',
      fixed: 'right',
      width: 100,
      render: (_,record) => <Space size={4}>
        <Button type="text" icon={<EditFilled style={{color:"#163E6C"}} onClick={(e) => editMachine(e,record)}/>}/>
        <Button type="text" icon={<DeleteFilled style={{color:"#163E6C"}} onClick={(e) => deleteMachine(e,record)}/>}/>
      </Space>,
    }
  ]

  const onRowClick = (record:IMachinesColumns) => {
    return {
      onClick: () => {
        if(isAuthorized){
          setActiveTab("TO");
          setExternalFilters([{
            field: 'machine_id',
            operator: '=',
            value: record.id,
            label: record.machine_number,
            isExternal: true
          }])
        }
      },
    };
  }

  const getMachineColumns = (columns:ColumnsType<IMachinesColumns>) => {
    if(isAuthorized){
      !canMutate && columns.pop();
      return columns;
    }
    return columns.slice(0,10)
  }


  const editMachine = (e:React.MouseEvent<HTMLElement, MouseEvent>,record:IMachinesColumns) => {
    e.stopPropagation();
    setEntityModel(prevState => ({
      ...prevState,
      machines:record,
    }));
    setIsShowEntityModal("machines");
  }

  const deleteMachine = (e:React.MouseEvent<HTMLElement, MouseEvent>,record:IMachinesColumns) => {
    e.stopPropagation();
    confirm({
      title: 'Удалить машину?',
      icon: <CloseCircleFilled style={{color:"#D20A11"}}/>,
      content: `Вы действительно хотите удалить машину ${record.machine_number}?`,
      okText: 'Удалить',
      okType: 'danger',
      cancelText: 'Отмена',
      onOk() {
        return new Promise<void>((resolve) => {
          axiosInstance.delete(`machines/${record.id}`).then(() => {
            resolve();
            refetchMachines();
          })
        }).catch(() => messageApi.error('Что-то пошло не так. Пожалуйста, попробуйте еще раз.'));
      },
      onCancel() {},
    });
  }

  useEffect(() => {
    setFilters(prev => [...prev.filter(filterItem => !filterItem.isExternal), ...externalFilters])
  },[externalFilters])

  useEffect(() => {
    if(refetchEntity){
      refetchMachines();
      setRefetchEntity(false);
    }
  },[refetchEntity])

  return (
    <>
      {isAuthorized &&
        <MachinesFilters
          filters={filters}
          isFiltersOpen={isFiltersOpen}
          setFilters={setFilters}
          externalFilters={externalFilters}
        />}
      {isMachinesFetching && <FullscreenLoader text="Загружаем данные о машинах..."/>}
      {!isMachinesFetching && !machines.length && <EmptyTable text="Нет данных о машинах" icon={<EmptyMachinesIcon/>}/>}
      {!isMachinesFetching && machines.length > 0 && <Table
          columns={getMachineColumns(machinesColumns)}
          dataSource={machines}
          onRow={onRowClick}
          scroll={{ x: 'max-content'}}
          rowKey="machine_number"
          pagination={{pageSize: 5}}
          loading={isMachinesFetching}
      />}
      {contextHolder}
    </>
  );
};

export default MachinesTable;