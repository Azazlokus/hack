import React, {useEffect, useState} from 'react';
import {Button, message, Modal, Space, Table} from "antd";
import {ColumnsType} from "antd/es/table";
import {useUser} from "@/app/store/UserContext";
import {TReferences} from "@/helpers/types/types";
import {useEngineModels} from "@/helpers/api/useEngineModels";
import {useAxleModels} from "@/helpers/api/useAxleModels";
import {useTransmissionModels} from "@/helpers/api/useTransmissionModels";
import {useSteerableAxleModels} from "@/helpers/api/useSteerableAxleModels";
import {useTechnicalServiceTypes} from "@/helpers/api/useTechnicalServiceTypes";
import {useFailureComponents} from "@/helpers/api/useFailureComponents";
import {useRepairMethods} from "@/helpers/api/useRepairMethods";
import {useEquipmentModels} from "@/helpers/api/useEquipmentModels";
import {CloseCircleFilled, DeleteFilled, EditFilled} from "@ant-design/icons";
import {instance as axiosInstance} from "@/helpers/utils/axiosConfig";
import {IReferences} from "@/helpers/types/interfaces";
import {stringSort} from "@/helpers/utils/scripts";

type referenceEditProps = {
    isEdit:boolean;
    id:number,
    name:string,
    description:string
}
interface ReferenceTablesProps{
    activeTab: TReferences;
    setRefetchEntity:React.Dispatch<React.SetStateAction<boolean>>;
    refetchEntity:boolean;
    setReferenceEdit: React.Dispatch<React.SetStateAction<referenceEditProps>>;
    setIsShowEntityModal:React.Dispatch<React.SetStateAction<boolean | 'references'>>
}

interface ReferenceTablesColumns extends IReferences{
    key: React.Key;
}


const ReferenceTables = ({activeTab, setRefetchEntity, refetchEntity, setReferenceEdit, setIsShowEntityModal}:ReferenceTablesProps) => {
    const [page,setPage] = useState(1);
    const [filters,setFilters] = useState([]);
    const {user} = useUser();
    const { confirm } = Modal;
    const [messageApi, contextHolder] = message.useMessage();
    const isAuthorized = !!user?.role;
    const canMutate = user?.role === "Администратор" || user?.role === "Менеджер";
    const tableHooksProps = {
        enable:isAuthorized || !!filters.length ,
        page,
        filters,
        limit:100,
    }
    const {engineModels,isEngineModelsFetching, refetchEngineModels} = useEngineModels(tableHooksProps);
    const {transmissionModels, isTransmissionModelsFetching, refetchTransmissionModels} = useTransmissionModels(tableHooksProps)
    const {axleModels, isAxleModelsFetching, refetchAxleModels} = useAxleModels(tableHooksProps)
    const {steerableAxleModels, isSteerableAxleModelsFetching, refetchSteerableAxleModels} = useSteerableAxleModels(tableHooksProps)
    const {technicalServiceTypes, isTechnicalServiceTypesFetching, refetchTechnicalServiceTypes} = useTechnicalServiceTypes(tableHooksProps)
    const {failureComponents, isFailureComponentsFetching, refetchFailureComponents} = useFailureComponents(tableHooksProps)
    const {repairMethods, isRepairMethodsFetching, refetchRepairMethods} = useRepairMethods(tableHooksProps)
    const {equipmentModels, isEquipmentModelsFetching, refetchEquipmentModels} = useEquipmentModels(tableHooksProps)


    function editReference(e: React.MouseEvent<HTMLSpanElement>, record: ReferenceTablesColumns) {
        e.stopPropagation();
        setReferenceEdit({
            isEdit:true,
            id:record.id,
            name:record.name,
            description:record.description
        })
        setIsShowEntityModal("references");

    }

    const deleteReference = (e:React.MouseEvent<HTMLElement, MouseEvent>,record:ReferenceTablesColumns) => {
        e.stopPropagation();
        confirm({
            title: 'Удалить запись?',
            icon: <CloseCircleFilled style={{color:"#D20A11"}}/>,
            content: `Вы действительно хотите удалить ${record.name}?`,
            okText: 'Удалить',
            okType: 'danger',
            cancelText: 'Отмена',
            onOk() {
                return new Promise<void>((resolve) => {
                    axiosInstance.delete(`${activeTab}/${record.id}`).then(() => {
                        resolve();
                        setRefetchEntity(true);
                    })
                }).catch(() => messageApi.error('Что-то пошло не так. Пожалуйста, попробуйте еще раз.'));
            },
            onCancel() {},
        });
    }

    const referenceColumns:ColumnsType<ReferenceTablesColumns> = [
        {
            title: 'Название',
            dataIndex: 'name',
            key: 'name',
            width:'30%',
            sortDirections: ['ascend' , 'descend'],
            sorter: (a, b) => stringSort(a.name,b.name),
        },
        {
            title: 'Описание',
            dataIndex: 'description',
            key: 'description',
            width:'60%',
            sortDirections: ['ascend' , 'descend'],
            sorter: (a, b) => stringSort(a.description,b.description),
        },
        {
            title: 'Действия',
            dataIndex: 'actions',
            key: 'actions',
            width:'10%',
            render: (_,record) => (
                <Space size={4}>
                    <Button type="text" icon={<EditFilled style={{color:"#163E6C"}} onClick={(e) => editReference(e,record)}/>}/>
                    <Button type="text" icon={<DeleteFilled style={{color:"#163E6C"}} onClick={(e) => deleteReference(e,record)}/>}/>
                </Space>
            )
        }

    ]

    const isTableData = () => {
        switch (activeTab) {
            case "engine-models":
                return {data: engineModels, loading: isEngineModelsFetching, refetch: refetchEngineModels};
            case "transmission-models":
                return {data: transmissionModels, loading: isTransmissionModelsFetching, refetch: refetchTransmissionModels};
            case "axle-models":
                return {data: axleModels, loading: isAxleModelsFetching, refetch: refetchAxleModels};
            case "steerable-axle-models":
                return {data: steerableAxleModels, loading: isSteerableAxleModelsFetching, refetch: refetchSteerableAxleModels};
            case "technical-service-types":
                return {data: technicalServiceTypes, loading: isTechnicalServiceTypesFetching, refetch: refetchTechnicalServiceTypes};
            case "failure-components":
                return {data: failureComponents, loading: isFailureComponentsFetching, refetch: refetchFailureComponents};
            case "repair-methods":
                return {data: repairMethods, loading: isRepairMethodsFetching, refetch: refetchRepairMethods};
            case "equipment-models":
                return {data: equipmentModels, loading: isEquipmentModelsFetching, refetch: refetchEquipmentModels};
            default:
                return {data: [], loading: false, refetch: () => {}};
        }
    }

    const getReferenceColumns = (referenceColumns:ColumnsType<ReferenceTablesColumns>) => {
        !canMutate && referenceColumns.pop();
        return referenceColumns;
    }
    useEffect(() => {
        if(refetchEntity){
            isTableData().refetch();
            setRefetchEntity(false);
        }
    },[refetchEntity])

    return (
        <>
            {contextHolder}
            <Table
                columns={ getReferenceColumns(referenceColumns) }
                dataSource={isTableData().data}
                scroll={{ x: 'max-content' }}
                rowKey="id"
                pagination={{pageSize: 5}}
                loading={isTableData().loading}
            />
        </>
    );
};

export default ReferenceTables;