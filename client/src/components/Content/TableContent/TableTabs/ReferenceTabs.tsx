import React from 'react';
import {Tabs} from "antd";
import {TReferences} from "@/helpers/types/types";
import ReferenceTables from "@/components/Content/TableContent/Tables/ReferenceTables/ReferenceTables";

type referenceEditProps = {
    isEdit:boolean;
    id:number,
    name:string,
    description:string
}
interface ReferenceTabsProps{
    activeTab: TReferences
    setActiveTab:React.Dispatch<React.SetStateAction<TReferences>>;
    setRefetchEntity:React.Dispatch<React.SetStateAction<boolean>>;
    refetchEntity:boolean;
    setReferenceEdit:React.Dispatch<React.SetStateAction<referenceEditProps>>
    setIsShowEntityModal:React.Dispatch<React.SetStateAction<boolean>>
}

const ReferenceTabs = ({activeTab,setActiveTab, setRefetchEntity, refetchEntity, setReferenceEdit, setIsShowEntityModal}:ReferenceTabsProps) => {

    const items = [
        {
            label: `Модель двигателя`,
            key: 'engine-models',
        },
        {
            label: `Модель трансмиссии`,
            key: 'transmission-models',
        },
        {
            label: `Модель ведущего моста`,
            key: 'axle-models',
        },
        {
            label: 'Модель управляемого моста',
            key: 'steerable-axle-models',
        },
        {
            label: 'Вид ТО',
            key: 'technical-service-types',
        },
        {
            label: 'Узел отказа',
            key: 'failure-components',
        },
        {
            label: 'Способ восстановления',
            key: 'repair-methods',
        },
        {
            label: 'Модель техники',
            key: 'equipment-models',
        }
    ]

    /**
    const getCreateButton = (role:TRole | undefined,activeTab:TEntity) => {
        if(
            ((role === "Администратор" || role === "Менеджер") && activeTab === "machines") ||
            (role !== "Клиент" && activeTab === "complaint") ||
            (activeTab === "TO")
        ) {
            return <Button
                className="primary-gradient"
                icon={<PlusOutlined/>}
                onClick={() => setIsShowEntityModal(activeTab)}
            >
                Добавить {entityName[activeTab]}
            </Button>
        }
        return null;
    }
     **/

    return (
        <>
            <Tabs
                activeKey={activeTab}
                items={items}
                onChange={(key: TReferences) => setActiveTab(key)}
            />
            <ReferenceTables
                setIsShowEntityModal={setIsShowEntityModal}
                setReferenceEdit={setReferenceEdit}
                setRefetchEntity={setRefetchEntity}
                refetchEntity={refetchEntity}
                activeTab={activeTab}
            />
        </>
    );
};
export default ReferenceTabs;