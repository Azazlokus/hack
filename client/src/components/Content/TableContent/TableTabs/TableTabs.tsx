import React, {useEffect, useState} from 'react';
import {Button, Space, Tabs, TabsProps} from "antd";
import {IBackendFilters} from "@/helpers/types/interfaces";
import {CaretDownFilled, CaretUpFilled, PlusOutlined} from "@ant-design/icons";
import {useUser} from "@/app/store/UserContext";
import {entityName, referenceName} from "@/helpers/constants";
import {TEntity, TReferences, TRole} from "@/helpers/types/types";
import MachineIcon from "@/ui/icons/MachineIcon";
import TOIcon from "@/ui/icons/TOIcon";
import ComplaintIcon from "@/ui/icons/ComplaintIcon";
import ReferenceIcon from "@/ui/icons/ReferenceIcon";

interface TableTabsProps {
    activeTab: "machines" | "TO" | "complaint" | "users" | "references";
    referenceActiveTab: TReferences;
    setActiveTab: React.Dispatch<React.SetStateAction<"machines" | "TO" | "complaint" | "users" | "references">>;
    setExternalFilters: React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
    isFiltersOpen: boolean;
    setIsFiltersOpen: React.Dispatch<React.SetStateAction<boolean>>;
    setIsShowEntityModal: React.Dispatch<React.SetStateAction<TEntity | boolean>>
}

const TableTabs = ({
                       activeTab,
                       setActiveTab,
                       referenceActiveTab,
                       setExternalFilters,
                       isFiltersOpen,
                       setIsFiltersOpen,
                       setIsShowEntityModal
                   }: TableTabsProps) => {
    const {user} = useUser();
    const isAuthorized = !!user?.role;
    const [viewportWidth, setViewportWidth] = useState(window.innerWidth);

    useEffect(() => {
        const handleResize = () => {
            setViewportWidth(window.innerWidth);
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);


    const items: TabsProps["items"] = [
        {
            label: `Машины`,
            key: 'machines',
            icon: <MachineIcon style={{fontSize: '24px'}}/>
        },
        {
            label: `ТО`,
            key: 'TO',
            icon: <TOIcon style={{fontSize: '24px'}}/>
        },
        {
            label: `Рекламации`,
            key: 'complaint',
            icon: <ComplaintIcon style={{fontSize: '24px'}}/>
        },
        {
            label: 'Справочник',
            key: 'references',
            icon: <ReferenceIcon style={{fontSize: '24px'}}/>
        }
    ]

    const getCreateButton = (role: TRole | undefined, activeTab: TEntity) => {
        if (
            ((role === "Администратор" || role === "Менеджер") && activeTab === "machines") ||
            (role !== "Клиент" && activeTab === "complaint") ||
            (activeTab === "TO") ||
            ((role === "Администратор" || role === "Менеджер") && activeTab === "references")
        ) {
            return <Button
                className="primary-gradient"
                icon={<PlusOutlined/>}
                onClick={() => setIsShowEntityModal(activeTab)}
            >
                Добавить {activeTab === 'references' ? referenceName[referenceActiveTab].toLowerCase() : entityName[activeTab]}
            </Button>
        }
        return null;
    }

    const tabPosition = viewportWidth < 768 ? 'left' : 'top';

    return (
        <Tabs
            activeKey={activeTab}
            items={items}
            onChange={(key: "machines" | "TO" | "complaint") => {
                key === "machines" && setExternalFilters([]);
                setActiveTab(key)
            }
            }

            tabPosition={tabPosition}
            tabBarExtraContent={
                isAuthorized &&
                <Space size={8}>
                    {getCreateButton(user?.role as TRole, activeTab)}
                    {activeTab !== 'references' &&
                        <Button
                            type="text"
                            icon={isFiltersOpen ? <CaretUpFilled/> : <CaretDownFilled/>}
                            onClick={() => setIsFiltersOpen(prev => !prev)}
                        >
                            Все фильтры
                        </Button>}
                </Space>
            }
        />
    );
};

export default TableTabs;