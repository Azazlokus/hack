import React, {useState} from 'react';
import {Button, Input, Space} from "antd";
import classes from "@/components/Content/TableContent/style.module.scss";
import {placeholderInput} from "@/components/Content/TableContent/constants";
import {SearchOutlined} from "@ant-design/icons";
import {IBackendFilters} from "@/helpers/types/interfaces";

interface TableSearchProps {
  setExternalFilters: React.Dispatch<React.SetStateAction<IBackendFilters[]>>;
}
const TableSearch = ({setExternalFilters}:TableSearchProps) => {
    const [search,setSearch] = useState('');
    return (
      <Space size={8}>
          <Input
            className={classes.content_form_input}
            placeholder={placeholderInput}
            max={255}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            onPressEnter={() => {
                if (!search) {
                    setExternalFilters([])
                } else {
                    setExternalFilters([
                        {
                            field: 'machine_number',
                            operator: '=',
                            value: search,
                            isExternal: true
                        }
                    ]);
                }
            }}
          />
          <Button
            className="primary-gradient"
            icon={<SearchOutlined/>}
            onClick={() => {
                if (!search) {
                    setExternalFilters([])
                } else {
                    setExternalFilters([
                        {
                            field: 'machine_number',
                            operator: '=',
                            value: search,
                            isExternal: true
                        }
                    ]);
                }
            }}

          >
              Поиск
          </Button>
      </Space>
    );
};

export default TableSearch;