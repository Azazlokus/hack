import React, {useLayoutEffect, useRef} from 'react'
import CustomButton from '@/ui/button/CustomButton'
import Tg from '@/assets/Tg.svg'
import Logo from './img/Logo.svg'
import LogoAdaptive from './img/Logo_a.svg'
import BtnIcon from '@/assets/ButtonIcon.svg'
import classes from './style.module.scss'
import {buttonText, number} from '@/components/Header/constants';
import {Link, useNavigate} from 'react-router-dom';
import {useUser} from "@/app/store/UserContext";
import {AuthService} from "@/components/Content/AuthContent/AuthForm/api/authService";
import {setAuthToken} from "@/helpers/utils/axiosConfig";
import {LogoutOutlined, ToolOutlined, UserOutlined} from "@ant-design/icons";
import {Button} from "antd";
import gsap from "gsap";

const Header = () => {
    const {user, setUser} = useUser()
    const navigate = useNavigate();
    const isAdmin = user?.role === 'Администратор';
    const headerRef = useRef<HTMLElement>(null);
    const logOut = () => {
      setUser(null);
      navigate('/login');
      AuthService.logout()
        .finally(() => {
          setAuthToken(null)
        })
    }

  useLayoutEffect(() => {
    gsap.from(headerRef.current, {
      opacity: 0,
      y: -100,
      duration:0.75
    })
  },[])


    return (
        <header className={classes.header} ref={headerRef}>
          <div className={classes.logo_wrapper} onClick={() => navigate('/')}>
            <Logo/>
          </div>
          <div className={classes.logo_adaptive} onClick={() => navigate('/')}>
            <LogoAdaptive/>
          </div>

            {isAdmin && !window.location.href.includes('admin') && (
                <Link to='/admin'>
                    <Button type="text" icon={<ToolOutlined className={classes.admin_button_icon}/>}>
                        <span className={classes.admin_text}>Панель администрирования</span>
                    </Button>
                </Link>
            )}
            <div className={classes.header_section}>
                <div className={classes.header_section_info}>
                    <Tg className={classes.header_section_sotial}/>
                    <span className={classes.header_section_number}>{number}</span>
                </div>
                {!user ?
                    (
                        <Link to='/login'>
                            <CustomButton extraClass='primary-gradient'>
                                <BtnIcon/>
                                {buttonText}
                            </CustomButton>
                        </Link>
                    )
                    :
                    (
                        <CustomButton
                            extraClass={classes.auth_button}
                            onClick={logOut}
                        >
                            <div className={classes.auth_button_content} >
                                <UserOutlined className={classes.auth_button_icon}/>
                                <div className={classes.auth_button_info}>
                                    <span className={classes.auth_button_role}>{user?.role}</span>
                                    <span className={classes.auth_button_name}>{user?.user.name}</span>
                                </div>
                            </div>
                            <div className={classes.auth_button_focus} >
                                <LogoutOutlined className={classes.auth_button_icon}/>
                                <span>Выйти</span>
                            </div>
                        </CustomButton>
                    )
                }
            </div>
        </header>
    );
};

export default Header;