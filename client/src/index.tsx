import App from '@/app/App';
import {BrowserRouter} from 'react-router-dom';
import '@/app/styles/index.scss';
import {ConfigProvider} from 'antd';
import ruRU from 'antd/lib/locale/ru_RU';
import {UserProvider} from "@/app/store/UserContext";
import {createRoot} from "react-dom/client";

const rootElement = document.getElementById("root");
const root = createRoot(rootElement);

root.render(
    <BrowserRouter>
        <ConfigProvider
            locale={ruRU}
            theme={{
                token: {
                    colorPrimary: '#D20A11',
                },
                components: {
                    Button: {
                        colorPrimary: '#D20A11',
                        colorLink: '#D20A11',
                        colorLinkHover: '#ff8687',
                        colorLinkActive: '#b60004',
                    },
                    Table:{
                        rowHoverBg:"#FFFDF5"
                    }
                }
            }}
        >
            <UserProvider>
                <App/>
            </UserProvider>
        </ConfigProvider>
    </BrowserRouter>,
);


