import React, {createContext, useState, useContext, useEffect} from 'react';
import {TUserData} from "@/components/Content/AuthContent/AuthForm/types/types";
import {jwtDecode} from "jwt-decode";

interface ProviderProps {
    children: React.ReactNode;
}

const UserContext = createContext<{ user: TUserData | null; setUser: React.Dispatch<React.SetStateAction<TUserData | null>> } | null>(null);
export const UserProvider = ({ children }: ProviderProps) => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        const token = localStorage.getItem('access_token');
        if (token) {
            const decodedToken = jwtDecode(token);
            setUser(decodedToken as TUserData)
        }
    },[])
    return (
        <UserContext.Provider value={{ user, setUser }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUser = () => {
    const context = useContext(UserContext);
    if (context) {
        return context;
    }
};