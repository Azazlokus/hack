import React, {useEffect} from 'react';
import {Route, Routes, useNavigate} from 'react-router-dom';
import HomePage from '@/pages/HomePage/HomePage';
import AuthPage from '@/pages/AuthPage/AuthPage';
import {checkTokenExpiration} from "@/helpers/utils/tokenUtils";
import {useUser} from "@/app/store/UserContext";
import {Modal} from "antd";
import AdminPanelPage from "@/pages/AdminPanelPage/AdminPanelPage";
import {setAuthToken} from "@/helpers/utils/axiosConfig";
import {TUserData} from "@/components/Content/AuthContent/AuthForm/types/types";
import {jwtDecode} from "jwt-decode";



const App = () => {
    const {setUser} = useUser();
    const navigate = useNavigate();
    const [modal, contextHolder] = Modal.useModal();
    useEffect(() => {
        const token = localStorage.getItem('access_token');
        setAuthToken(token);
        checkTokenExpiration(setUser, navigate,modal);
        const tokenCheckInterval = setInterval(() => checkTokenExpiration(setUser, navigate,modal), 60000);
        return () => clearInterval(tokenCheckInterval);
    }, []);
    return (
      <>
        {contextHolder}
        <Routes>
            <Route path='/' element={<HomePage/>}/>
            <Route path='/login' element={<AuthPage/>}/>
            <Route path='/admin' element={<AdminPanelPage/>}/>
        </Routes>
      </>
    );
};

export default App;