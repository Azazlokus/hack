import React from 'react';
import {Spin} from "antd";
import classes from './styles.module.scss';
interface FullscreenLoaderProps{
  text:string;
}

const FullscreenLoader = ({text}:FullscreenLoaderProps) => {
  return (
    <div className={classes["fullscreen-loader"]}>
      <div className={classes["loader-block"]}>
        <Spin tip={text} size={"large"}>
         <></>
        </Spin>
      </div>
    </div>
  );
};

export default FullscreenLoader;