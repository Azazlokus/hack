import React from 'react';
import classes from './styles.module.scss';

interface EmptyTableProps{
  text:string;
  icon:React.ReactNode;
}
const EmptyTable = ({icon,text}:EmptyTableProps) => {
  return (
    <div className={classes['empty-table-wrapper']}>
        <div className={classes['empty-block']}>
          <div className={classes['empty-icon']}>
            {icon}
          </div>
          <p>{text}</p>
        </div>
    </div>
  );
};

export default EmptyTable;