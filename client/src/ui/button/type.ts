import {ReactNode} from "react";

export interface ButtonProps {
    children: ReactNode;
    onClick?: () => void;
    type?: "button" | "submit" | "reset";
    disabled?: boolean;
    extraClass?: string;
    htmlType?: "button" | "submit" | "reset";
}