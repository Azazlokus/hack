import React from 'react';
import {Button} from 'antd';
import {ButtonProps} from '@/ui/button/type';

const CustomButton: React.FC<any> = ({children, htmlType, extraClass, disabled, onClick}) => {
    return (
        <Button
            htmlType={htmlType}
            className={extraClass}
            disabled={disabled}
            onClick={onClick}
        >
            {children}
        </Button>
    );
};

export default CustomButton;