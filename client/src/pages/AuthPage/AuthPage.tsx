import React from 'react';
import Container from "@/components/Container/Container";
import Header from "@/components/Header/Header";
import Footer from "@/components/Footer/Footer";
import AuthContent from "@/components/Content/AuthContent/AuthContent";

const AuthPage = () => {

    return (
        <>
            <Container>
                <Header/>
            </Container>
            <Container>
                <AuthContent/>
            </Container>
            <Footer/>
        </>
    );
};

export default AuthPage;