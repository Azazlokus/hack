import React, {useEffect} from 'react';
import Container from "@/components/Container/Container";
import Header from "@/components/Header/Header";
import Footer from "@/components/Footer/Footer";
import AdminContent from "@/components/Content/AdminContent/AdminContent";

const AdminPanelPage = () => {
    return (
        <div>
            <Container>
                <Header/>
            </Container>
            <Container>
                <AdminContent/>
            </Container>
            <Footer/>
        </div>
    );
};

export default AdminPanelPage;