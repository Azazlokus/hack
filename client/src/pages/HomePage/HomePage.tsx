import React, {useEffect} from 'react';
import Header from '@/components/Header/Header';
import Container from '@/components/Container/Container';
import Footer from '@/components/Footer/Footer';
import HomeContent from "@/components/Content/HomeContent/HomeContent";
import {useUser} from "@/app/store/UserContext";
import {TUserData} from "@/components/Content/AuthContent/AuthForm/types/types";
import {jwtDecode} from "jwt-decode";
import TableContent from "@/components/Content/TableContent/TableContent";

const HomePage = () => {
    return (
        <>
            <Container>
                <Header/>
            </Container>
            <Container>
                <HomeContent/>
                <TableContent/>
            </Container>
            <Footer/>

        </>
    );
};

export default HomePage;